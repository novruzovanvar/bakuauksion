function tinymce_setup_callback(editor)
{

    editor.settings.toolbar = 'removeformat | styleselect bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | link image table | code';

    editor.settings.menubar = 'file edit view insert format tools table help';
    editor.settings.image_advtab = true;
}

tinymce.init(window.voyagerTinyMCE.getConfig());
