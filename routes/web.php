<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[\App\Http\Controllers\SiteController::class,'index']);
Route::get('/test',[\App\Http\Controllers\SiteController::class,'test']);

Route::get('/auctions/{category_slug}',[\App\Http\Controllers\SiteController::class,'auctions'])->name('auctions');
Route::get('/all-auctions/',[\App\Http\Controllers\SiteController::class,'allAuctions'])->name('allAuctions');
Route::get('/archive/auctions/',[\App\Http\Controllers\SiteController::class,'archiveAuctions'])->name('archive.auctions');
Route::get('/for-army-posts/',[\App\Http\Controllers\SiteController::class,'forArmyPosts'])->name('forArmyPosts');
Route::get('/for-army-posts/{slug}',[\App\Http\Controllers\SiteController::class,'forArmyPost'])->name('forArmyPost');
Route::get('/partners/{id}',[\App\Http\Controllers\SiteController::class,'partner'])->name('partner');
Route::get('/partners',[\App\Http\Controllers\SiteController::class,'partners'])->name('partners');
Route::get('/qalereya',[\App\Http\Controllers\SiteController::class,'gallery'])->name('gallery');

Route::get('/auctions/{category_slug}/{slug}',[\App\Http\Controllers\SiteController::class,'auction'])->name('auction');

Route::get('/page/{slug}',[\App\Http\Controllers\SiteController::class,'page'])->name('page');

Route::get('/kateqoriya/{slug}',[\App\Http\Controllers\SiteController::class,'categoryPosts'])->name('category.posts');
Route::get('/contact-us',[\App\Http\Controllers\SiteController::class,'contactUs'])->name('contact');
Route::get('/for-army/subscribe',[\App\Http\Controllers\SiteController::class,'forArmySubscribe'])->name('forArmySubscribe');
Route::post('/message',[\App\Http\Controllers\SiteController::class,'message'])->name('message');
Route::post('/subscribe',[\App\Http\Controllers\SiteController::class,'subscribe'])->name('subscribe');

Route::get('/elan/{id}',[\App\Http\Controllers\SiteController::class,'auctionById']);
Route::get('/post/{slug}',[\App\Http\Controllers\SiteController::class,'post'])->name('post');
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    //Asset Routes
    Route::get('voyager-assets', [\App\Http\Controllers\Controller::class,'assets'])->name('voyager.voyager_assets');

});
