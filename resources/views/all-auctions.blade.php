@extends('layout.master')

@section('meta')
    @include('layout.base.meta')
@stop

@section('content')
    <!-- breadcrumb-section start -->
    <nav class="breadcrumb-section theme1   " style="padding:50px 0; background: url(https://sfera.az/file/pic/xeber/2017-01/1485250146_baku-tower11new.jpg) no-repeat; background-size: cover;">
        <div class="container-xl">
            <div class="row mb-3">
                <div class="col-md-4">
                    <ul class="list-group">
                        <li class="list-group-item d-flex  align-items-center border-0 bg-transparent text-light">
                            <img src="/template/svg/003-bank.svg" style="height: 40px; margin-right: 15px;">
                            <span class="category-header">
                                Daşınmaz Əmlak
                            </span>
                        </li>
                        @foreach($sections->where('slug','dasinmaz-emlak')->first()->children as $row)
                            <a href="{{route('allAuctions', ['category_id' => $row->id])}}" class="list-group-item d-flex justify-content-between align-items-center ">
                                <span >{{$row->name}}</span>
                            </a>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-group">
                        <li class="list-group-item d-flex  align-items-center border-0 bg-transparent text-light">
                            <img src="/template/svg/002-delivery.svg" style="height: 40px; margin-right: 15px;">
                            <span class="category-header">Nəqliyyat</span>
                        </li>
                        @foreach($sections->where('slug','neqliyyat')->first()->children as $row)
                            <a href="{{route('allAuctions', ['category_id' => $row->id])}}" class="list-group-item d-flex justify-content-between align-items-center ">
                                <span >{{$row->name}}</span>
                            </a>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-group">
                        <li class="list-group-item d-flex  align-items-center border-0 bg-transparent  text-light">
                            <img src="/template/svg/001-diamond.svg" style="height: 40px; margin-right: 15px;">
                            <span class="category-header">Daşınar Əmlak</span>
                        </li>
                        @foreach($sections->where('slug','dasinar-emlak')->first()->children as $row)
                            <a href="{{route('allAuctions', ['category_id' => $row->id])}}" class="list-group-item d-flex justify-content-between align-items-center ">
                                <span >{{$row->name}}</span>
                            </a>
                        @endforeach
                    </ul>
                </div>
            </div>


        </div>


    </nav>
    <div class="d-none d-md-block">
        <!-- breadcrumb-section end -->
    @include('layout.sections.next-auctions',['nextAuctions' => $nextAuctions])
    <!-- breadcrumb-section end -->
    </div>



    <!-- product tab start -->
    <div class="product-tab bg-white pt-40 pb-80">
        <div class="container-xl">
            <div class="pb-5">@include('layout.sections.filter')</div>

            <div class="row">
                <div class="col-lg-12 mb-30">

                    <!-- product-tab-nav end -->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- first tab-pane -->
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                             aria-labelledby="pills-home-tab">
                            <div class="row grid-view theme1">
                                @foreach($posts as $post)
                                    <div class="col-sm-6 col-lg-3 col-xl-3 mb-30">
                                        <div
                                            class="card popular-card popular-card-bg zoom-in d-block overflow-hidden position-relative">
                                            <span class="badge bg-info  price-badge position-absolute">{{str_replace(',',' ',number_format($post->price))}} AZN</span>

                                            <div class="card-body">
                                                <a href="{{route('auction',['category_slug' => 'elanlar', 'slug'=> $post->slug])}}"
                                                   class="thumb-naile">
                                                    <img class="d-block mx-auto"
                                                         src="{{Voyager::image($post->thumbnail('cropped'))}}"
                                                         alt="{{$post->title}}">
                                                </a>

                                                <h5 class="card-title">
                                                    <a href="{{route('auction',['category_slug' => 'elanlar', 'slug'=> $post->slug])}}"> {{$post->title}}  </a>

                                                </h5>
                                                <p class="pl-3 text-left"><small>Tarix: {{date('d.m.Y', strtotime($post->date) )}}</small></p>
                                            </div>
                                        </div>
                                        <!-- product-list End -->
                                    </div>
                                @endforeach

                            </div>
                        </div>

                    </div>

                    {!! $posts->links('vendor.pagination.custom') !!}
                </div>


            </div>

        </div>

        @include('layout.sections.brands',['partners' => $partners])
    </div>

    <!-- product tab end -->
@stop
