@extends('layout.master')

@section('meta')
    @include('layout.base.meta')
@stop

@push('css')
    <link rel="stylesheet" href="/plugin/lightbox/css/lightbox.min.css">

@endpush

@push('js')
    <script src="/plugin/lightbox/js/lightbox.min.js"></script>
@endpush

@section('content')

    <!-- breadcrumb-section start -->
    <nav class="breadcrumb-section theme1 bg-light pt-50 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center mb-15">
                        <h2 class="title text-dark text-capitalize">Qalereya</h2>
                    </div>
                </div>

            </div>
        </div>
    </nav>
    <!-- breadcrumb-section end -->

    <!-- product tab start -->
    <div class="product-tab bg-white pt-80 pb-80">
        <div class="container-xl">
            <div class="row">
                @foreach($gallery as $row)
                    <div class="col-md-3 mb-25">

                        <div class="product-thumb">
                            <a href="{{Voyager::image($row->image)}}" data-lightbox="roadtrip" title="{{$row->title}}"> <img
                                    src="{{Voyager::image($row->thumbnail('cropped'))}}"
                                    style="width: 100%;"
                                    title="{{$row->title}}"
                                    alt="product-thumb"></a>
                        </div>

                    </div>
                @endforeach
            </div>

        </div>
    </div>


    <!-- product tab end -->
@stop
