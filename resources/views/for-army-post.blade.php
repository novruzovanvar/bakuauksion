@extends('layout.master')

@section('meta')
    @include('layout.base.meta',[
    'title' => $post->seo_title,
    'description' => $post->meta_description,
    'image' => $post->image
])

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5fa4e64fed8380c6"></script>
@stop

@push('css')
    <link rel="stylesheet" href="/plugin/lightbox/css/lightbox.min.css">
    <style>
        .breadcrumb-item a{
            color: #000 !important;
            font-size: 16px;
        }
    </style>
@endpush

@push('js')
    <script src="/plugin/lightbox/js/lightbox.min.js"></script>
@endpush

@section('content')
    <nav class="breadcrumb-section theme3 bg-light pt-50 pb-50" style="background: url('/assets/img/bg_2.png');">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb bg-transparent m-0 p-0 align-items-center justify-content-center">
                        <li class="breadcrumb-item"><a href="/">Əsas səhifə</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{route('forArmyPosts')}}">Ordumuza dəstək</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{$post->title}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </nav>
    <section class="product-single theme3 pt-60" style="font-size: 16px;">
        <div class="container-xl">
            <div class="row">
                <div class="col-lg-6 mb-5 mb-lg-0">
                    <div class="position-relative">
                        <span class="badge badge-danger top-right"></span>
                    </div>
                    <div class=" mb-20">
                        <div class="single-product">
                            <div class="product-thumb">
                                <a href="{{Voyager::image($post->image)}}" data-lightbox="roadtrip">
                                    <img src="{{Voyager::image($post->image)}}" alt="product-thumb">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="flex-container  align-content-stretch  d-flex flex-wrap">
                        @foreach(json_decode($post->images) ?? [] as $image)
                            <div class="single-product mr-1 mb-1">
                                <div class="product-thumb">
                                    <a href="{{Voyager::image($image)}}" data-lightbox="roadtrip"> <img
                                            src="{{Voyager::image($image)}}"
                                            style="width: 100px; height: 100px; object-fit: cover;"
                                            alt="product-thumb"></a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="col-lg-6 mt-5 mt-md-0">
                    <div class="single-product-info">
                        <div class="single-product-head">
                            <h2 class="title mb-20">{{$post->title}}</h2>
                            <div class="star-content mb-20">
                                <span class="star-on"><i class="ion-ios-star"></i> </span>
                                <span class="star-on"><i class="ion-ios-star"></i> </span>
                                <span class="star-on"><i class="ion-ios-star"></i> </span>
                                <span class="star-on"><i class="ion-ios-star"></i> </span>
                                <span class="star-on"><i class="ion-ios-star"></i> </span>

                            </div>
                        </div>
                        <div class="product-body mb-40">
                            <div class="d-flex align-items-center mb-30">
                                <h6 class="product-price mr-20"><span class="onsale">{{str_replace(',',' ',number_format($post->price))}} AZN</span>
                                </h6>

                            </div>
                            <p>{{$post->excerpt}}</p>
                            {!! $post->body !!}

                            <p class="help-text text-uppercase">Əlaqə</p>
                            @foreach(explode(',',setting('site.phone')) as $phone)
                                <p class="title text-dark">
                                    <a href="tel:{{$phone}}">{{$phone}}</a>
                                </p>
                            @endforeach

                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox mt-5"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
