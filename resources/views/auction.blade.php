@extends('layout.master')

@section('meta')
    @include('layout.base.meta',[
    'title' => @$auction->seo_title,
    'description' => @$auction->meta_description,
    'image' => $auction->image
])

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5fa4e64fed8380c6"></script>
@stop

@push('css')
    <link rel="stylesheet" href="/plugin/lightbox/css/lightbox.min.css">
    <style>
        .table tr td:nth-child(1){
            background: #eee;
        }
    </style>
@endpush

@push('js')
    <script src="/plugin/lightbox/js/lightbox.min.js"></script>
@endpush

@section('content')
    {{--<nav class="breadcrumb-section theme3 bg-light pt-50 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb bg-transparent m-0 p-0 align-items-center justify-content-center">
                        <li class="breadcrumb-item"><a href="/">Əsas səhifə</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{route('auctions',['category_slug' => $auction->category->slug])}}">{{$auction->category->name}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{$auction->title}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </nav>--}}
    <section class="product-single theme3 pt-60" style="font-size: 16px;">
        <div class="container-xl">
            <div class="row">
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <div class="position-relative">
                        <span class="badge badge-danger top-right"></span>
                    </div>
                    <div class=" mb-20">
                        <div class="single-product">
                            <div class="product-thumb">
                                <a href="{{Voyager::image($auction->image)}}" data-lightbox="roadtrip">
                                    <img src="{{Voyager::image($auction->thumbnail('cropped'))}}" style="width: 100%;" alt="product-thumb">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="flex-container  align-content-stretch  d-flex flex-wrap">
                        @foreach(json_decode($auction->images) ?? [] as $image)
                            <div class="single-product mr-1 mb-1">
                                <div class="product-thumb">
                                    <a href="{{Voyager::image($image)}}" data-lightbox="roadtrip"> <img
                                            src="{{Voyager::image($image)}}"
                                            style="width: 100px; height: 100px; object-fit: cover;"
                                            alt="product-thumb"></a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="col-lg-8 mt-5 mt-md-0">
                    <div class="single-product-info">
                        <div class="single-product-head">
                            <h2 class="title mb-20">{{$auction->title}} &nbsp; &nbsp; <span
                                    class="badge badge-danger ">@lang('site.'.$auction->done)</span></h2>
                            <div class="star-content mb-20">
                                <span class="star-on"><i class="ion-ios-star"></i> </span>
                                <span class="star-on"><i class="ion-ios-star"></i> </span>
                                <span class="star-on"><i class="ion-ios-star"></i> </span>
                                <span class="star-on"><i class="ion-ios-star"></i> </span>
                                <span class="star-on"><i class="ion-ios-star"></i> </span>

                            </div>
                        </div>
                        <div class="product-body mb-40">
                            <div class="d-flex align-items-center mb-30">
                                <h6 class="product-price mr-20">
                                    <span class="onsale">İlkin s/q: {{str_replace(',',' ',number_format($auction->price))}} AZN</span>
                                </h6>
                                <p>
                                    <span>({{$auction->price_text}})</span>
                                </p>

                                @if($auction->discount)
                                    <span class="badge position-static bg-dark rounded-0">Endirim {{$auction->discount}}</span>
                                @endif
                            </div>

                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox mt-5"></div>
                        </div>

                    </div>
                </div>
            </div>

            <div>
                @if($auction->price)
                    <table class="mt-1 table table-bordered mt-3">
                        <tr>
                            <td>Lot No:</td>
                            <td>{{$auction->lot_number}}</td>
                        </tr>
                        <tr>
                            <td>İlkin s/q: </td>
                            <td>{{str_replace(',',' ',number_format($auction->price))}} AZN</td>
                        </tr>
                        <tr>
                            <td>Endirim </td>
                            <td>{{$auction->discount}}</td>
                        </tr>
                        <tr>
                            <td>
                                Əmlak (təsviri, miqdarı, mülkiyyətçisi və digər sahibi):
                            </td>
                            <td>
                                {!! $auction->body !!}
                            </td>
                        </tr>
                        <tr >
                            <td>Mülkiyyətçi:</td>
                            <td>{{$auction->owner}}</td>
                        </tr>
                        <tr >
                            <td>Sifarişçi:</td>
                            <td>{{$auction->customerRel->name ?? $auction->customer}} {{ $auction->customer_organ }}</td>
                        </tr>

                        <tr>
                            <td>Vəziyyəti:</td>
                            <td>{{$auction->condition}}</td>
                        </tr>

                        <tr>
                            <td>Hərrac addımı:</td>
                            <td>{{$auction->auction_step}}</td>
                        </tr>

                        <tr>
                            <td>Beh:</td>
                            <td>{{$auction->depozit}}</td>
                        </tr>

                        <tr>
                            <td>İştirak haqqı:</td>
                            <td>{{$auction->participation_fee}}</td>
                        </tr>

                        <tr>
                            <td>İştirak üçün tələb olunan sənədlər:</td>
                            <td>{{$auction->required_docs}}</td>
                        </tr>

                        <tr>
                            <td>Sənədlərin və behin son qəbul tarixi:</td>
                            <td>
                                            <span class="badge position-static bg-info rounded-0">
                                        {{ date('d.m.Y',strtotime($auction->finish_date))}}
                                        </span>
                            </td>
                        </tr>

                        <tr><td>Hərracın keçirilmə tarixi:</td>
                            <td>
                                            <span class="badge position-static bg-success rounded-0">
                                        {{ date('d.m.Y',strtotime($auction->start_date))}} {{ $auction->start_time }}
                                    </span>
                            </td>
                        </tr>

                        <tr>
                            <td>Hərracın keçiriləcəyi ünvan:  </td>
                            <td> {{$auction->address}} </td>
                        </tr>

                        <tr>
                            <td>Əmlakın yerləşdiyi ünvanı:  </td>
                            <td> {{$auction->object_address}} </td>
                        </tr>

                        <tr>
                            <td>Sifarişçinin rekvizitləri</td>
                            <td>{!! $auction->customer_requisite !!}</td>
                        </tr>

                        <tr>
                            <td>
                                Hərrac təşkilatının rekvizitləri:
                            </td>
                            <td>
                                {!! setting('site.requisite') !!}

                                <p>
                                    <strong>Facebook:</strong>
                                    <a href="{!! setting('site.facebook') !!}" target="_blank" class=" btn-link">{!! setting('site.facebook') !!}</a>
                                </p>

                                <p>
                                    <strong>Tel:</strong> {!! setting('site.phone') !!}
                                </p>

                                <p>
                                    (Əlavə məlumat üçün hərrac mərkəzinə müraciət edə bilərsiniz).
                                </p>

                            </td>
                        </tr>

                    </table>
                @endif

            </div>
        </div>
    </section>
@stop
