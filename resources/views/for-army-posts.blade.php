@extends('layout.master')

@section('meta')
    @include('layout.base.meta')
@stop

@section('content')

    <!-- breadcrumb-section start -->
    <nav class="breadcrumb-section theme1 bg-light pt-10 " style="background: url('/assets/img/bg_2.png');">
        <div class="container-xl">
            <div class="row mb-3">
                <div class="col-6">
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img
                                    src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFhUXGBoaGBgYFxkYGxsfGxoXHR0YGhgeHSggHx4lHxgfITEhJSkrLi4uGiAzODMtNygtLi0BCgoKDg0OGxAQGy0lICYvLTUvLS01LS81LTUtLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOAA4QMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAFBgMEBwACAQj/xABGEAACAQIEAwYDBAgDBwQDAQABAhEAAwQSITEFQVEGEyJhcYEykaGxwdHwBxQjQlJicuEVgpIWM1OTssLxVKLS4kNzwyT/xAAbAQACAwEBAQAAAAAAAAAAAAABAgADBAUGB//EAC8RAAICAQMDAwMDAwUAAAAAAAABAhEDBBIhEzFRBSJBMmHBFHGRUoGxBhWh0fD/2gAMAwEAAhEDEQA/ANxrq6uqEOrPe2mF/ajzX6gn7iK0Kk7t0AuRzp4on+of/SteiyKGZNsx6/BLNgcIK34QqcF4xcw1zQ6aSp2Yef3Ny9NK07hXE7d9M6HyIO6noR+ZrLMXYzCRvyP55V54Nxd7D5lMEaEHY/yt5dD+T1dXo451vh9X+Tg6H1Ceml08n0/8o2OgvHeDd74lPi6HY7fI1Z4NxZMQmZdxoy81PT06HnV88q8/KLTpnp/ZkjfdMz9LRUlWEEbg19uqCCrAFToQdQab+L8LW8vRxsfuPlWc8S7RWbLtbdpKkhsviAI3E7H2mqpQbZzsmCcJXAmwuDW3bvYZif1W9qG3Nl9IYjmkga8o160LW2yjJd+NdJBkMAAQyt+8CpBnnr0MFuH8QS4M9pww8vsI3B9aFdpsGRbZ7UjZionQgznToRJmNwxq3S6l6ae5fwdDqx9Tj+k1TptqpeJdrf7rh+eP3KpDIZBIPIiieC7VX7XMEekH7I+lLOD44CMt3Q/xgae4G3qPlV8KrCVII6gyK9Hi1On1ceKf2+Tzuv8ARdf6XOssXXxJdn/f8MdeC9vC94W7iAK2gbYz0Osfnzp5w99XUMpBB5isMe0QZEggyCORFNv6NeM3O/fD3DKuDcUncMIzD0IIMfymsWt0cYx3wNPp2ulKfTyO77GgY3BK48+oMEeYI2PnS7iMfiMM3jPe2urCCPIsNB6nQ8jTbUOIw4Ya/mdwRzHlXMhPbw1aOzPGpO7aflf+oHYXitq74VYBv4W0Pt19qtmxSpxfs0BJsrEa92ZYR1Tn7DbzoJf7S38OuVXPimM0PliPhJ9efyq1bJOoOn4f4ZVknPFzL3R8rhr90/w2aA9g16W1WNXu0uIzZu+eeuYj6be1MXZTtlda8lu82dbjBZMSCxgEQNpI0qyWKSV2Uw1cJOqo0fuhXC2BUgt18K1ns20RM4HKomuTvU71AVp40KzyYr4K+kV9BpwUfK6vWauoWTaE66urqzFp1Kv6QrGbCueaw3yYfcTTVQvtHh+8sXE/iRl+akD60H2LtPPZljLw0Yf+uOhlWI8uXuNqn/xAPqRDfQ/hQ3F3Aok0ExOPI209KbRajUY5boPjwz03+odP6ZlgoaiFza4ce6/v+OTQuD8Xew6ujeWuxH8DeXn99anwji6YhAyaEEBlJ1U9PToedYB2Yx73c1siY6eem3typ/7F4h1a7lk/sGZGiRKlSAev3xXX1SxajF1VxJHzjBHLo9R0fqg+z/P/AGh87Y4lreBxVxTDLZuEHpCnX2rATwK5cBZ7tq1/K7eIeqgae5nyrb27QWruFcYgG2WQoyFSZzKRKgjUa+3Osaxww4utcZfiV850aSTGik+czNcXJKWP2/J39PGOXkg7DXDZxqjvFe1dDoWUnLKqXWQQDIymP6jHOtIxbCNCD9aykXpU27KKGnTxaAgEh1Ouo005GaM9luP4i8SLqHKQMrBYEjcH1+2fKq3cuTBr9NJS3xK/HcELV3w/A2q+XVfafrVK25UypIPUGKY+OYU3FEfEpkefUfnoKTsQ7EwuwjUkASeUkgVijhbzVF0e79N9dhP0vfl90o+1rz4v+wctcYuDfK/qIPzH4Uy9gseHx+HEFTL85B/ZXNJ+vtWYX3ZSdQY3hlb7Ca1X9CXDu9uNiW2tDIp6s0yfUL/1111lzwW3fui/Jws0fTdRGeR4FDIlaceLd+DZBX2uoF2l7VWMFl74mWDEBRJ8McvOdOsGqzmBPiGBW8uVi68wyOyMI10ZTPLbY86yn9IfALlq5h1W4XRu8OYxmEZJz8jvvp95NcS7dvdss1j9nKyCBmYHlOYZR9aTsNxXvO9N1nNx/EGZiVIAEhTsrGJI0k7dKWblBbl3GxRhkmoy7CpiLl0Ehk1B3VgR9tG+wt0nGWjctuyo2YKP3ioJ9dIDadNdJoRiOIi4xBgEcp1PvRHh1u4EZgcrAAgAjMFMyQRryEjzo/rMm25GnH6XppZYwi6bfz8G/cM4ravgm24JHxCdR5EVYdawHBcUupcW4LrBxHimTHQk7jyOlbbwzioZF71kBKBwwIyOsDxqTy11HL0IJGPIp8o1ep+lS0W23aZcYVS4lihattcIJCgmF1J8hXrD8Xs3QWRpSSuePCSDBg8wDpO2hpN7c8RuYhv8OwcNdcE3WJhUUfxMAYEx7wNdRV8JJnJkmu4c4H2htYmMsglA46FTsRsSPOIOsGi4uClnsz2Ds4Rxezvdu5MudyIEkliigaTPMn11JLXZw8b1ZaE5+DvnXVYgV1JuDRZrq6uqkc6oMYPCfKD8qnqLEkBTO0VGRGDcR4C5um2VJXORIPJSdD0mImhT8OdrJZcNh1AIXMfGYMnOGJ9p9Kbu310wy4drrRL3Ah7sKDJJ7zz/AITMx5Vnz8XfIogyDmidSIAzbQdI0jnWWG5LjydbPqHqJR61JpVwUOIDug2RypMB1WQrRmEETtOsHTWtC7FcXAwoCsCVQBiPikfukdOY9azi9ckxGpIOvQDb5xRe1gouDujlbLuJ1JBCiOTEg6xEAzVmT3Q5Zz1NRyUkN2P4l3txlzAQYWTGmUEa9STSRiSAMw8YAJYBj4TzkjbXWKIYt7a2TeuNF8sQygiJXTRdthMjQkz5VY4MiXcKf2YhQxBhSZ1IM7kwR8qWbUYKw47eRtC5guIFbvem2CMrKqhogkaNzJiNvOm3gfEES2oOXmxZtd2J0AqrxXg9m1ZNwQcqk/AusDbagtnHWWt2xc/aZREL+zKgAbsAS0+0RRwyUlaQmojL5Y+cJ4paewpZczCZiZlnMLI3358gKDcTXDpiu87os4CsozBYbxaNykkT91J1nibBDbS4UGbfMQQNdBGpGp0/EmvXD3Ob9khZVkuxkT6lT4fIAzvqaCg02x4uPHAVwyWkVR3S51IzHMHDK0jloCCdR+RsP6P8fhsHgQgJJDuWY5VkkkjxEgaLC/5axaGuksVSxbtjxtmJ0JGgBJZien1FaVwniGHs2kUNOVAc2WTLBWblp4n28taMsjijRHBHI2jS+BdpMPih+yuoWEyguIzCDE+EnTzrMO1HHDev3DkLJOUQf4djJke3nU/DGNzG2sVYKWwr5bzGPGucggxuQojX+Ma6Uv8AF4YrnLE8lVSdeZMaDXmatxzUjJnx9OVA/D49mt6sBkZxGgjxMQSDrueu3SgOM4uGXIFzLmPPQ+LTTy9+tV+OZbd1xmzKygxJknaNDGkbn5VSw15EUFmBLHURMDr91WMoSo9PaZ8pCmBO7MRA1MTsPMaSaNDiV0ZLtsKAmxUz4WyyrTup0nTf50c7NG09prr5cqj95ZB9BI56azuOtU+MIhR0tqtsudkUKOREgcjz+dDjsH7ha3iLdy2rvZto7LJ7suAJ2MZsoPtVPFcWyWSgdjq2VWbwgxAYbQdvSlrg/B8ViHa1ZCs4QuQGGoG+XmW/lGuhq3esG0YxAVyICorSGbkGOkATqPOKChFPg2P1DUuGxzbX35/yapw3jlsIER1yjwoR8LZbWZiDsZedee9M/YrA2Ut3LltVzvcY3CDMkbDyA/h2EnrWOcNwpvW2a6zMbbHMqABD/KG6qAJiNMonStt7E8OW1hbcLlzqGg7wdgfalxwire7nwJmyylCNR488cfbyGCs18CxVkpXnuhVu4zUQZq6p+6FdU3IlEldXV1IMdS12nxdwutm2oLMBEmBPiPiPIAKTTLWe/pKvKly2+Z0KpJZQxBEkQI2I1nqDFLPsWYpbZWIHabiW6ZhDlgwBkNlyLmHlmBFKV20WtL4T+yZ/EP4SM0HyliJ84iiHGcfauHQZgpJza6yNoI6iZ33oNgsec0FsqnNvMGREGkjGocIac3KdtkaWct3x+HTrPprR/AcQCq5zANpLMICqIEjXUmNB5ztQPFYtVeRleVAzb5SOY5ExtNH+BYNrtsOUIU+LMULQFkFs22YxlHSCfVckl0/cCMbycC7xzErchhJOhYxA56KoAhRtJ1NV8Jxe7bUqjEKTtp5ag+gFMXGrOdW8ZyIWZ/C0ADkNMvKN+dKdnEALMCZ1/MGjjanGqBO4ytEmK4k7iGJM7+cbD0qLDYVnVmUjwQSs+IjXVRziNfUV5aDr6018J4Phxg2v3X7t7p/YsDLIFJVnK5vEpIIIiRAPOQzagiQi8kuSH9Gt+wMaP1iyl60UYFHUMNSpzBToWET86ZeO461ne3h7K2le8FCWbYUwTHw9cqyZpDsOmHxC5LguKrDxqCN94Bjr6GDTHwh//wDXYa4wAuDOQBAlrZ8I5A7n2HWmyS9vC+BIxe/l+CPA8ItveGbOSrEwUyyFMAZp22mdZonxVsOLuUkrCHOttj42JTKPc86h7QcRFm+QuqFASPMsxPOl233t/E2yqy7uMiyJIEEjTlH31XHmA8pSjkNf4Xh+7yLK5MokldydTlHITseQoD2swclC0lXYhlBkSNix1Gq+2nUzV18Va1AtXMyiTm8BbqVlo+cH0oT2nxpXC3G0OggFdAZEAnMZ+dZcTanZoyU40Ll/hOHf+IMCQIIyxJ3B189Nalw3Yz9Zb9jdSDooYHUgagNmOg6geVLT8Zc6ED02+Z331ojZ4u5toLbZDaYklSJhsx8JG2unvXQd/BiilfI2W+zOJw9pUe2zBSfgGdeubwyeZ3Aj3NL3FrpSWaZG+n0Io72d7eu6smKJZljKygDNofiEgSCBt1oT24vW71wXE8IIAcT0Oh+UDfkNKqhklv2yRryaeHT6kGDMNiDaIvK7KROqmGOuwI/I0rzibzZ2dmzOdjM5ZEk67uZ33Gp3Iifs9wu5jMVZsIILnwyCQi7m6wHIDbzjaa3Ts/8AotwWGuLdJuXnUyO9Klc38WUKBPPWfoKv4MoscP4Q2GsWrV23kOSSp5k+Jo1MmW19a1+2oAgbUD7YWE7g3GElCCv+Y5Y981W+CcTF1BPxRrpE+Y/POs8EoSavvyBUnVhOurq6rgnV1dXVCHV1dXVCHVmf6XLjFrahigVGZmAmcx0BOwHhO/WtMrMf0n4tO+VCTmRAYA1I11k6ACeoO9LJ0gxVsxTiNoq+sQY2AHIRIXQGNae+DfosNzh74y4zm41o3LNhCq6RKl2ZTqRrlEbxPSl2d7LjG3SZVbKmX1AuMCfhUKAonmY0kfFR/j3aO+2a33VxlQZClu4mUAaZTbmSNuRmleVIs6bZn+D7I3XXPdYWU/dzDxETuRyHrryjnTAcP3NtbdoSAIzAySdSDBME6zAjc+1biHaxWGtu93g0IcArpygEH2MR51LY4SL1sX7rsj3BIS14YUjTNOaSRrAjess5zlzLhFsYxj9PLF7tBxFriEG4xEGFMjLMwvQ+tLdiw7AlUZgNyqkx6wNKYuJ2VXQOx/hzRr02ArTMFwpMPZVAMsDWOvMzzmj1ljjwgvFvfLMYs4Zv4GNezgrp2R4+XKn/AInibYcgoZ5kGNPMcx5ivvAWS87qADEHyE6R57H8mg9VKr2hWmXkSMC1zDXWzQOR1B5gz8vtr3i7xuMMkGTAGkCADPQAaa1ovF+zuGt2i7JJJjYASTyEbUkYhAiMQqrtDJcEGeqa8teVXYZ9T3UV5YbFQX7McBsXkc37hPiMOjwFygGOcz4tTpoI3o9hOPYXDKEwiKeQBOTXbU5CSTPTXr1QcDiryq7AjxqbZXaNRLEE7xmE8pq7wBQ103mZStqDKzq7TlA01OhgdYpckHbcnwSE0ktq5Hy/hMWwzZbamdV74s3/AEAA/L1pV45iAC1o5gWVc1tgRBzBhsYIOXcEjTemlOJgEK2h/eAMwY2PmOdLHa26jrnUarqDOoHMa8uf4TVOBpzVosyXsdALiPD7Pdl5VbgI8OYnMPPXQjQ/OqeExo0QKFGxgcztPvVLE3m1Bqq7knzJ+2uijCGRhDmoqmEZwQx33MfdXYLEKyqSQCRJ+oJ+YojbetCgnyP3R44PbuWDmUkNJ8algT5zyMch096fuCdusVbEO4ugcnGv+oQfnNJ3BeCPicUFtsQe7uEDkWVGIU66AmBtXrDzGbrUxxVbWJldK0Ge1HbnEXMQQxNu1ClUWGETIuyR4ySJgjQaQGBpw7D8TTElVe5lf4kyHw3AN4O421XfQ+dY9xGxcY5balnZgAoEkkmAB5mmzsp2R4hbxaW2V7SKyu9wqxtiBMqdAW2WQd+oBpMmGDq+67FMHu5N6FdWQ9vP0oNm/V8A4ETnvwD7W5BEfzQZ5da8fo7/AEjvmGHxlw3M7Ql07qTACttKzz3E9Nl6Uqsvs2GuqPNXykololrq6uoBOpE7Z4JTfNy40IUVRlJDNqwyk8lBMyDJnkAQXukD9IurhRp4AT0jMw2qvJ9JbhaU1YgYziDAqEi0chzuszJbRdTJyqNT1NL3FMmVdCSNSzBcxJ5sxifuonxa7cyC3bUaTM6+evMDnM0oYq9cIzBvKJJ+pJrPFWaJ5PBe4ZY724AIaTLEsfCOZ108tPSm3GXslsoBrEA8x0j2oR2Csutu5c6tHPXkB7HMflVnjWJYOzARppA02qnLzLb4BB0rFK9cJv2gZ0YGD5EMfnArT8fjC6K0wMs6iRPn5HrII0rI8W7M/eeYI9op/wCDYzvMPbIBYBcrcoywpH53p9RFqMWTDJbmhZ7SXRowJDa5lM6R0bYg/Py0on+ju1c8VwFCjEAqG8axMkiNoM7zFXuMcIsXEy5spOgO5HSROqj6UpdksTcs4pRbjMxyHmPiE6dYBAnrRjWTE0hcm6M0zQO2d4PZHiMpqcs8wRETESefQdKz/C8SZXRgBKOHXyMKAc0yCAqx0yiiOPxL20y7oMyq3NgGIk9dZ1oIridRT4riq+A5WpNeRr4f2ltAMzWlLy0soBdpJJJJ82Jqrxbiz3GISVCwyknXMw3HSFkDzM0ts0MGGnUeVXswzAj94DYAbQBHy386jgrsCdppBjBQQUB+FCw89Qsn+pszehFeG1RuUqR1Gumo8vur5hUU2b12YzFEQeSz9w+lUHxEoR1+Wkz9Y+dLH6m0F1tpgDFMZ1EH8JH3V4s2yT9TU2OvZ3J9vlUr4O7acLctvbLLIFxSpKkmGAIGkg6+RrbHsZGknwfcSfh/pH2mus4p12Yj3rxilgx0A+wVGtWIBpv6HeIXG4laUwQVuSekIx+0Cp+2163h8diLI0AcMPLvFW5Htnj0ilDsp2hbBu11R4+6urbgDwu65Qx5kDePxodxPGPduM7u1x2gs7MWY+EDUneAAParLp2JJbuBqtY0Aq9sjMpDKehBkH2ImtM/SDxy/f4ZY/U1ObGFUbKYKqVZnWdl1UoWJAAnUViOEuaA0ewuLvX7H6tnIsgnQgZdXz7RL+LXXSjklFJSZViUlLagHwrh9y6xQTmDZSOQ5GSD5fSjuN4FZtA5rtwMIBEDWTGaQNFg9feaN8GwQs/DdbzXIMp9ht60LxnC8diC57lzaRbj95EWyEDnRzCkkjKNdz61jjnnLJw+DpqGJYuVyOH+0GL/APWP/ps//CupC/2cxX/B/wDfb/GuqzdD+pGfoZP6X/B+na6urqAp1Jn6QbEZLmpBBQ+0sP8AupzoT2owouYZ/CWyjOFUEk5dYAGpJE6UmRXFoaDppmPYvDm6wkCDpPLqZ+tJ+KwNpbmXSC20kb6Ez08qaMTibF0sq3QGMgoSbbCYkAGNDpI+yk/jwJJfOr5RGhAIE6ArMiJ3rJCL3V2NLce4xcDtZbORbgy53W2s6ElsxPsCAPWh/GseqHuyfXTTw8p66UDw6C6rs4ByqTJgQTMAGDrPLTbcaVTwlvNdy5iwiYJmTvG5nr66U60+6W5sR5VEuPZAUacqvdnMebZa3ycg9dev0FV7hqpZIDgmYBBMbxIkD2rTkhuhtEUtrsd8XhgUi4dG9DB6gxoaVMLwy4mNyowZ0GcEDmwAUkctXBPvVrE9pCkhbWYMTkkxGux31qfsjj2xF293rhAq5yFISFkBvFoY0WZNYscMmOLbRfKcMjSCvHcIiYZbIEvpAiScoI+pMfOkJG8UHbUGeUdflTVxXtSiBhZDOToLrQqr0Nu3EgQYBJG860kW3GcTsTr6Hc/WaswY5JPcLmyLigkFRphgDyVj9hqG3/vFQEbgDXQT59BXrifDmtrIOZeZiCD6dKo2UGYZpjy39K0LHXco6lq0NYvK2W3b1t2/3uTMd4ncaQPnQ7jsJov70kazpzPltFVXv3bagTK75TJAPkd9qu9meN2LOJW/irBxAtjwIGCrmBJBeQcwBO32xFLDE1K/geWRONVyCOI8Ou2WC3rbIzKrgMIJVhKkDoauHity/e7/ABLG+wXKBcJiMpVR4SIC/FAjUa7mZu1HHL2OvHFX/jYwAJyoo+FFHQSfUknnRTgnBbF3AXWLZMUuIy2pMK691ndWB2CqjNm32GsgVoKGK7nWvq17K15K1BVLg9tcMAcht7xP2fSpralgBHXX5afT6mobaHrVsOw5/QUUrI5E6iBFN/ZtT3A2ysTt5GNSPTakZsSdtPXpRPhPG2w5Uhs6bOhIzAdV11I3E+nOqtSnONIOB7ZWx+wihfMzy/P3VYPaa4lp7NtgEcqXJGbKAy5sv9SgjXkZ3oLjuLlodWRgQGVlWDrMKTME1VwgaTK92h2BJ8I/qI1/O+9Z8OJ1bL8uXmo/Ayf4ja8/+c9dSz+oD/ij/UPwrqH6WP3L/wDcZ+D9JV1Bb3ajDrsWf+lDHsxhT86oXO2S/u2W/wAzKP8ApzVqMdDTUaX0JIDKSNwCCR6jlSnd7ZmD+xX/AJpH/wDOssx/DL7XWKumSTlZmbvADyLKon1jWjRC7+lnhKW8d3s2it0Sy2wocQqBjcEEkmZBPInaJrKcTaAOms7U83+ybsSTiVE7juyfrmH2VA3Y9Zk3z7KB9pNQZJgGxg2WwrsvxGQT05Hy011oWC6sH1+Kc0zPvNN2M4DeLR357qAAo026jY+tWrPZ2xlAYSeep/GgHY2AMQAwDLz3qFbUVBfxTM7LYAFsNCzGo2kljz302Bonw7DtcU6Qw+KNvZjp7TIp00UtSqkVcVa8O0wQRp51878oDbAWDv4RrqNZifwq/jcBdzAKbeU7Z3VST5QTOtUP8Eu6zcs+GZAcmORBAXrSydsaEWlyD8ffLALzELG3oaG3Ug/nqaYjwNyVZn5j4UuMdD0yzVx+zIdoNxh6W4HX4idaCG2sFd4Lq21zeKfFv0g8wNaqWbE3YBgA/ETtB+2mizwBApgXhl//AFgt8yY66xV3h/DUQQM6zuCUM+sCj3CoULuOQRsdOY8M/wCWI+lWuwuAsPic1y/YsBPErYpc9tjtGSVWRvDNHrFFL/Cw8xmWJ0aCD5ggmOuvyoMvZtycquBtuNd9T0MDXcTUXYEk1yeeP4Syl5rdi939ufDcW2bSFp8QVST4QdAQY08qG92czfDoY3B11+ESCfhOoEbTyrVv8N4PYwJtLYfEYk6C5ctujZ20zm4shEXfKCdBsxJolhb+C4Tw65cw961isaUVCxcK+pCju0IJyJObLGsEk01imJ374OmYafyffmqTh2FuX7nd2bT3XMwttWYx1gax58q+4bh2bkxNOfYTu8FibeKFx5WVe2FVswYQVB0IGxmOVRpixa7Dr2U/RFauYUNjFu2b5J0S4hheUgqyz6eVVu1P6KsPhcNdxP61cK2kZsrIhLHkoYFQJMDY71ofA+22GxLi2M1tz8K3ABm8gQSJ8qUf09cTZcPYwqnW87M3mtoAxP8AU6n/AC1FKS4C0jCL9qXMac/aBRLB8OV2kHwKvi5gmJIBgVSt3AW1MmeXl0AE044I93Z7tRDhM5DKGBJGbUHpovtSsVuisFQ5VYDTkAIDe9G8LYyqCxPi2UsD7wOVK2AuBriNOaCWjSNtWYbbxpRB+JFrjZQznQADlA/ec+c0LJfFB7uh0H+muoJ+vXf4bf8Azf7V9phaY1cQ7RWyRlcRHLXX2obc7Q2/4qE8f7P5EDYZTA+JMzMT/MuYk+o9IpUdLv8AA/8Apb8KnbhF9jy/aO31NVn7Sp50i3LjTB0I5c68Fz1qB3odbnaRagftGKTmbzr5mHWhTD1Br/2i8qjfj7GQOlLYdOZNeu/QbTQph6h8wl8KQDy29QQfuonhuNXLaBEVIkkZlJk78j91BXCk7x86+2A52nnHvvQE3UHf9oMSYAKg/wAtsE+0g1Hd4pixq1x1B2m2q/KQJqrZwlwaa6xRXF8AcWbbZS1x9coXNCk6fTX2NJvTdIXqc0gUOL32IUXmkkAaxqTGsetM97BIBK3bl2N5uXAdzrlDAQd9OtLdjhWIDKTZcAMDOWNjvRxdpq1cMtjz3IntWzus+rOftaobuDswT3YBg6gn8akv3OZ3+2qAxTEGT10gcx1iTVtxrsK+Ah2a4oZ7p2JH7pOsHp6Hl/ejL23IVx4WB1G4Mcpiffy6Uq4LhV7S4AMrCR4hMbjTrTTwl+9M7Mohp6HnHnH5iq0g7lXIXwN4OAw+2rVxLcHOWCwYIgiehB2naZ/tXw1hLc5dzqdfsGwqHiuM8BAIkiBOonzHSj0ZLlMxdeClwBMdhS662kysAd5Ouu4WhmAx62XKLbACTvrJmvV7jmJsiGthF2Aa0yg+S6gH2oeMTc79b3dENnUhcjQzaQADMkwNNZoI0rgYzczqQyQxMltdADooG3vvofKpe1fGr2Lw9mzeFtjZgJfhxcVYAbMcxDZgonTcTpUWAxjN4btprbHVQwKg+QUgR5VNjLBVTctqXX95f3l/H1oSmuzBdMBcGwyWwbj+KBoBrJ1jTmPKpBi/iLLdcsfCSVzBp3iTp5Ty5VNw97TElFZFB8WdlCajSDOgGs+URUbAjXMQpkhgNCDzUbwY3J9qSN8tsmRr4RWs4kW84ZSMyw0R1BiRttVa9jxoBJA2VRlA9TuarYi6CctoSRzJ1P41R75gd9fSmFSCPfr/AMEfOuqh+vP1rqlsambBi7wRSx/8+VUXwSraNy8C1xpgZmiYkKADAAgCetW7hnp5abeddawneHXYfmB0qzuF8KxExnCHusSFiDuNvQa1C/Z1wNvqK0nFW7Vu2WbwooJPoPvpd4NxVcRmBXKwkgcspJjXqBofQ+zJclMpt9hTXgL9Psr5b4KW1AkdeXt19dqZu0N7JltrqzCSOXlPXY6bdZ0pefFtP+8afImg3FMEFNq7Obs8++UD8+tRf4I38Iq7heNXFIk5x0bX6706fqOgJESAQCI0IkaehoraxZrIuwi2uzzEqIGpA366ffTbw3gSW0AKgtGpI+yrd3CQCeY1HqNR9RRl7QOo2OvzrHrLqohjua5BlnBqBMCvqeIs/U5V9Bp/f/NU2LxKJCs4SZgnQT67c9jXWrDLlKgOg2ysJ8t9PrS4IRxx57luJbe57uYVYjypDw7eAfnkKe3xag+KU/qGUf6vhPsTSKLYUsvQx9K0WXwfJSbdvUUOs4tcuXuzmJ8Tan+w5xVzieICDzNBjif70QSHngd0G0oIGggdYGmvuDRtFW2DlAEmT5mlDhvELdsLmJHhHxAgGCSNdv3jz6UYxWNuM/d2rTl+hERO0z61apxirbMueE51GKL+JxoFCbWKtd4DcuDfbSYnUjej3D+xveCcXfgn9xTlHzma9cR7M8Ks+DvIc7zeI+kzWZ62DdKy+HpeSrlQF7T4hjae3adWQGHRgrTAkOhI0I8o+2aN3Go+GsQUFxXtzB8YKmJ1JIk66QKsX8GqZshXu5AQDmIGo022Gus+lLXEcKUbMB4SfkenpTQ90bRbkg4PlGlYS7cdbtq4xZMhfK0EZgBBmM2x6/WhstbIO3LcHlsSNIP/AJpQ4DisSGPci6QAcwt5yoEahgNNuvlRfE8XuDwrrOmoB9KSXhiNWj12iw2ZRcVhlWZtkTqdiPSNqAYvv7ujBgAAAACJg8+tE7fEzn8WkaKBsKPdnrzNcm5kiDAyny11NGK2qilsTrHBLv7oqxb7PMDN64BJggCTykknoPI068bdQQmy6u3Qb6knkAPrS0MYXJZvhb4eXhggT8yfUnypiW6Ln+xdvqPkfxrq9f41c8vrX2jQtyGVVkwNztRNEAAFDsLiEU6nWo+N8ZFqwzrBbZfU6A+g39quSrkOSduhd7c8WzN+rofCpBeObcl9t46kdKh4aiYdA9zfeBuT0Hp+d6o8AwRuPnaTBmTuWO5Pnr8z5Uax+HS3isO+IRmwwK58vODLL67e1S6VlTW+W34FfiePa9cLtpOgA5AbCqlMXaHgEG5iMOyXMKXJVlcZlDGQjWzDgiY25Uu1Sa6o4ita4XeuX7Fm7ePi7tFnqFEAnzIE+9ZdgcC911toPExgfifIb1rhsm3ZCW4JVQqzzgRr8qqyT29hZHg2V6iosLcVUCswBWV1O+UlZ+lKOLNy1JbCvA5riHHvoPvrwnFEBBeziE9b1xl915+kVS5SfAl0M13E2Sxy4hASYKkqwJ2jKdflVa9w/IZW2VM6th2yH/lkwfcmlvgGDF3FXMQBop8OhGp56x5mnqxO5pHSYrfgF2sY48K3kf8AlvL3T+mYAA/6TSRx3Hd1dugrDFjpyEkmJ5jUVp920rCGUEeYBoPjuy+HuGSgBG2gI9IOkeVNDIoseMmjIL94sZYyajmtWvdnbaiP1aw401VAjR7yJ+VDrwS3ohNr+W4gj2cf/KtCyqTpB3lHgVtjbzNuZ9RJkyPztTxw1LT28wMFlyOBoVgbg7wdTPnSjbVmIzBSeRDDX0nUUS4AGa/4B4E8JLfvMVJyj8T98VNTFSh+xp0mZxnS+Q9geyeFS6twEuwMiTJ01kmfoIruMcHwr3ne4gkwdhB0Gh0qe/cm2QoUaa5geXSIg0Dwr3FzszZgRzzMT0lia5sZN82d7bFQuitxvDxZUhIUyVjoNBA6b+tLGKxK5JKltpgT7xU93il3MQQzIxYLEkaEgjyIiq9vD3CSQjRz8JrrY3tjRwc0tzs+YTGqVyJfur0VbhSD/Qd/aKpqtxWnPttOu3vp/avmMuIT4lB9d/nUoNtbZyZjMaMcwWJ2PIUWilEAXU+X5/vTDwDfOMxyHrqT/D77e9Klq6Q2uUg6GJphxF/ubQFv/eXP3huFGhYecaT69aVlckSce4qL7BUnL/8AkY6E5YhI6An8619s4RXUGdh8iDt6EazQTDvEDy5+ex+n0qbDYkqwg6c+hHSoLL7E3dv0H+pfxrqi/wAQt/8ACHyH4V1MLyG8Vxmy0MrzO4yOCD7rFR4gJftoEuqGNyCGDKFAUyzMwCx5TroOdL2HSPSnXssilDIEg07k9oFCMslhXs1w9PDbtspJ0HiB9zr6k+9aSnB7Xddy1sOhHiDCZPX1nWs7v8OtPqyKSOZA5dantYVB8Iy/0kr9kUjlZfDHtIO0/YjDKxFi4yHmp8ajyBkH5zQK12Pj4run8q/iaivpd7xgl66Ndu8c/aagxmMxVowbzj1VD9qmnSRVKbsceAcItWZKLrsWOp+f4UWas9wXaTEjTvFP9VsfdFXk7U4jmtk/5XH/AHmufndT5BvT+RpvIGYLyHiPt8I+ev8AlNc+DU8qWrPam4CZsK0mSRdK+QEFD061Z/2sXnYuf5Sh+0iqXTC5Jhezw9E+FQPQVM5CihNvtRZO63V9UB/6WNV73aTDk/7wj1Rx/wBtWYsalICSXYM2781MaXxxrD/8e2PV1X7YojY4ihGjK3owP2GtOXB8xGTstOaA4w5zlAmr+KxQgkmF6jX67fWqiXGZQAO7Qc4h2/D1OvpANDHi28sKVFHD4NLdxSy+EztyMHX10kelFMJct2yXSSAQWHNgsjOOsAnTTYdNbl9LIWySirmJmOXhZc/n0k9aXFLoxKnUHpIP9oqya3KmWY3sdobMTiCAb1gqwI1HI+fketBXxl26R3sKJAAHUmBJ96oYTiL2ScolTup+0dDy50Nx3GSxIRQsE6k68+m3rPSsEdNJS+x0/wBbFw57+ChwpmbPzti42m3xEke8HeivduuX9qQrTDAKPYgqdRz186r8MzKk27Qy3BDZiCBBiRHPpMbzRe0ua33cab+nKfs/JronJvnkA3uG9+GLsMw0D5QGPSYIBHlE9DVPE9l8XaUt3RdddU1Om8p8f0NX+G3gLgJ3DfYaabXaSMVdtsQqqRleCYIiZ6gkke1K212GXcymwk5ieQJ99h9T9Kns8QuAGcpgBZZZI6ANuDp6aa1pvaexhcQveXLWV2H+9tRD6SM5AE+ranlSda7OIwCrcYGZeQDGmwiI3jnvUUrC6QBuu2kKZGkjp0+ZqBrzdCK0PD9gWBAS8pQiczKQQYGkc+s6elQ4rsoyuqBg2baB69SNdKPAraET9b8/t/GvtNf+H4f+N/8ASK+UeCUgW2FZRJGm0gz/AH5UZ7NY3I8HY6f3qtmnfUc+h8qp3EKHQ6cj9x8xVzRlxy/k0lWr5cuZVJ0EdZilDA9oGUAGD60UXtAotu7D4RMdegHqYHvVLjRsjNMOYXhwzNc001g7yTGg561U7U4LNaMiGT5+Yqthe0p7qy9q0VYiGBOhIkArv4TGoPtUdq/d/V375szk77bxAA5AbAchUT5DJe1iazEVYtYrrVfFbmoVo5cUZ9zn9gqLwkVKLlDFNWLZrJPSwS4GLoeoMQsivYrlViQBsec/ZWPDGbn7fgMIOToiw3DM2rnKvTn/AGq1wzBrcvC1bAWQcxAE5RuSdSOXuRXnuu80zSB0iPeOe1Wuy1koXcNqTpBG0mPY5RXVt/Js2xguA6LCZlABKLsgMAx8P/ug19uYrSQACDt0/PnXu1fCnNEtOnT5Dn0oXYVzc/aeEuzTOmgO+XlQEbGLA4TOC9zxA6ifv8h0oDx/ENnLIgadN8u3PYzNRtxS4924gfNZttlW2igkgKNiB15sYqDG3rwGc2sqTEZsxE9Y8+vWoSmgdevYg7LaXmMwZvrIH0ocrXCMhQCATKzHuPwovcLMofSYMr0g8/t2qraxUOogyWC9fi0++ogPd8BS0FtW/E3hVRmI+7rPIUCxHFbrmFY2rZ0IU+IjoW316DSrPGrma4tgHwq2p5Ty+Q+tfRgU58vUffRJ2IXeVEaZQIy6R5j7akuOGFy65guxMDfnoB1NSNaEgDmYGn9694DAG5fyMVVUmSdusCgCMi7xTHD9VGGtGSpUFh8K6htG5nlp6cqp4K/3Y7tdWgzPL1868ca4kgixhoyKQcw6xBjz86jwltEWXPPU/dUDJh67xW5bCMHJ2EeQ3JFHVxaXbSOvnlI1K+E5vUiNvSkC/wAQBYnlsB5VTw3EijQjmNdjBGhG9EkUHs6f+ob/AEf/AHr7Sz+vj+IV1GywnwOKka78/wAff8atgBpU7H6eY8684fs3eUK0rmI1QmNwDlnUE/LWKitfnl/4PlT4ssZppGXNj2uyG5bKtB9j1HWiuG4O11YYEKYM9PMdarMoYQfY9D+B5/2o/wADx4Ki2+jLprzHKjNNBwtSZ6t2SrKgGiQB7RH0qxxy5CADnVlWUnlNe8XhxcWPlVSNbXAhYtOdVlFEuKYQoxBocNKsb4MMlTL2Hs6TVhLcaVe4JZtMhNyZ2EcvP3+6q/FsqF2ScqwJaASdNo5a71yJynOTRNrfYq3sRlmNYgehMxPyNeHfKQASclyHEQpIywPTQ+1UjiIDLqZAI1jocxPz09POK2Kv6yxLEgTO2ggaDoAPOt+OChGkbYxUVRau4rMDmhdfhQidvMjTfWnLspgblzDK4C6kqIOkLA315g7dKz44kA7ae3v9a07snxK2uDsgzJVjoBpLNvtrTtgm+C+ht2QczAv5CY8h0+lJvHOKOznJOXeNCdOc/cDTPj+JqQ2W2skbkAn5RSHjTGYa6bf2oFTfgK8I4+VtqqIucEhsxjNrqR/N7+2mpPimPNy3lL5CdDaEOx6Sf3R5b0ocLsK6asRqenXXcUQykABWYDbRiJ9hoPaoFyokaxkBaGZgD4V1iR+8eVeglwC2TaKhWzl20bSNI3iQOXOrrFgs/CNIUaAR+dzQ/FcVu3nIYwoRiRAEmDAJ+vsKgdxVszIP73xa66zI9dqujiasQWt93tJWWT1y7j0E1UQQwnnpVuzgGvOEWYMktBgabUbETPN/GWw82z3gU6tGVR/TJlj7AetL3E+JFrjNqJPWjvaHBHD24KwOuhBPqPvpMIooaKLCY5gZFfbmPc1VK18ollIlOIY8685j1rxX2oE+11fK6gQ//9k="
                                    class="card-img"  style="object-fit: cover; height: 200px; ">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body py-50">
                                    <h5 class="card-title">Dəstək üçün <span class="text-danger">AL!</span></h5>
                                    <p class="card-text" style="font-size: 16px;">Bu səhifədə yerləşdirilən elanlardan
                                        bir şey alaraq əsgərlərimizə və şəhid ailələrinə dəstək ola bilərsən.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img
                                    src="https://www.a3haber.com/wp-content/uploads/2020/09/azerbaycan-sikiyonetim-680x365_c.jpg"
                                    class="card-img" style="object-fit: cover; height: 200px; object-position: left;">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body py-50">
                                    <h5 class="card-title">Dəstək üçün <span class="text-danger">SAT!</span></h5>
                                    <p class="card-text" style="font-size: 16px;">Bu səhifədə elan yerləşdirməklə
                                        əsgərlərimizə və şəhid ailələrinə dəstək ola bilərsən.</p>
                                    <p class="card-text"><a class="btn btn-link p-0 text-capitalize"
                                                            href="{{route('forArmySubscribe')}}">Elan yerləşdirmək üçün
                                            müraciət</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-6 col-6">
                    <div class="card text-white bg-success ">
                        <div class="card-body text-center ">
                            <h5>0 <i class="icon-doc"></i> Elan</h5>

                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-6">
                    <div class="card text-white bg-success">
                        <div class="card-body text-center ">
                            <h5>0 <i class="icon-people"></i> İştirakçı</h5>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-6">
                    <div class="card text-white bg-success ">
                        <div class="card-body text-center ">
                            <h5>0 <i class="icon-basket-loaded"></i> Satılıb</h5>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-6">
                    <div class="card text-white bg-dark ">
                        <div class="card-body text-center ">
                            <h5>0 <span>AZN</span></h5>
                        </div>
                    </div>
                </div>
            </div>
            <form action="{{route('forArmyPosts')}}">
                <div class="row mt-10">
                    <div class="col-5">
                        <div class="form-group">
                            <select name="category_id" class="form-control">
                                <option value="">Bütün kateqoriyalar</option>

                                @foreach($categories as $section)
                                    <optgroup label="{{$section->name}}">
                                        @foreach($section->children as $sub)
                                            <option value="{{$sub->id}}"
                                                    @if($sub->id == request('category_id')) selected @endif>{{$sub->name}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <input name="search" class="form-control" value="{{request('search')}}"
                               placeholder="məsələn: iphone 7"/>
                    </div>
                    <div class="col-1">
                        <button class="btn bg-dark text-white form-control"><i class="ion-ios-search-strong"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </nav>
    <!-- breadcrumb-section end -->

    <!-- product tab start -->
    <div class="product-tab bg-white pt-20 pb-80">
        <div class="container-xl">
            <div class="mb-3" style="clear: both;"></div>
            <!-- product-tab-nav end -->
            <div class="row grid-view theme1">
                @foreach($posts as $post)
                    <div class="col-sm-6 col-lg-4 col-xl-3 mb-30">
                        <div class="card popular-card popular-card-bg zoom-in d-block overflow-hidden">
                            <div class="card-body">
                                <p class="text-center p-1 position-absolute"><span
                                        class="badge badge-success position-relative">{{str_replace(',',' ',number_format($post->price))}} AZN</span>
                                </p>

                                <a href="{{route('forArmyPost',['slug'=> $post->slug])}}"
                                   class="thumb-naile">
                                    <img class="d-block mx-auto"
                                         src="{{Voyager::image($post->thumbnail('cropped'))}}"
                                         alt="{{$post->title}}">
                                </a>
                                <h3 class="popular-title text-center">
                                    <a href="{{route('forArmyPost',['slug'=> $post->slug])}}"> {{$post->title}}  </a>

                                </h3>

                                <a href="{{route('forArmyPost',['slug'=> $post->slug])}}"
                                   class="btn shop-now-btn text-uppercase mt-25">Ətraflı</a>
                            </div>
                        </div>
                        <!-- product-list End -->
                    </div>
                @endforeach

            </div>

            {!! $posts->links('vendor.pagination.custom') !!}

        </div>

    </div>


    <!-- product tab end -->
@stop
