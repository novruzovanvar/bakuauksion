@extends('layout.master2')

@section('meta')
    @include('layout.base.meta')

@stop

@section('content')


    <!-- product tab start -->
    <div class="product-tab bg-white pt-40 pb-80">


        <div class="container-xl">
            <div class="row">
                <div class="col-lg-3">
                    @include('layout.sections.sidebar')
                </div>
                <div class="col-lg-9 mb-30">

                    <div class="pb-3">@include('layout.sections.filter')</div>
                    <!-- product-tab-nav end -->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- first tab-pane -->
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                             aria-labelledby="pills-home-tab">
                            <div class="row grid-view theme1">
                                @foreach($posts as $post)
                                    <div class="col-sm-6 col-lg-3 col-xl-3 mb-30">
                                        <div class="card popular-card popular-card-bg zoom-in d-block overflow-hidden position-relative">
                                            <span class="badge bg-info  price-badge position-absolute" >{{str_replace(',',' ',number_format($post->price))}} AZN</span>

                                            <div class="card-body">
                                                <a href="{{route('auction',['category_slug' => 'elanlar', 'slug'=> $post->slug])}}"
                                                   class="thumb-naile">
                                                    <img class="d-block mx-auto"
                                                         src="{{Voyager::image($post->thumbnail('cropped'))}}"
                                                         alt="{{$post->title}}">
                                                </a>

                                                <h5 class="card-title">
                                                    <a href="{{route('auction',['category_slug' => 'elanlar', 'slug'=> $post->slug])}}"> {{$post->title}}  </a>

                                                </h5>
                                                <p class="pl-3 text-left"><small>Tarix: {{date('d.m.Y', strtotime($post->date) )}}</small></p>

                                            </div>
                                        </div>
                                        <!-- product-list End -->
                                    </div>
                                @endforeach

                            </div>
                        </div>

                    </div>

                    {!! $posts->links('vendor.pagination.custom') !!}
                </div>
            </div>

        </div>



        @include('layout.sections.brands',['partners' => $partners])
    </div>



    <!-- product tab end -->
@stop
