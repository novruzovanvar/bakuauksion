<div>
    <aside class="left-sidebar mb-30 theme1">
        <!-- search-filter start -->
        <div class="search-filter">
            <div class="check-box-inner pt-0">
                <h4 class="title">Əmlaklar</h4>
            </div>

        </div>

        <ul id="offcanvas-menu2" class="blog-ctry-menu">
            @foreach($sections as $section)
                <li class=""><a href="javascript:void(0)">{{$section->name}}</a>
                    <ul class="category-sub-menu" style="display: none;">
                        @foreach($section->children as $sub)
                            <li><a href="{{route('allAuctions', ['category_id' => $sub->id])}}">{{$sub->name}}</a></li>
                        @endforeach
                    </ul>

                </li>
            @endforeach
        </ul>

    </aside>

    <div class="sidebar-widget mb-30">
        <h3 class="post-title">Növbəti hərraclar</h3>
        @foreach($nextAuctions as $date)
            <div class=" " >
                <div class="media-body">
                    <h5 class=" mb-1">
                        <a class="alert alert-success d-block" href="{{route('allAuctions',['start_date' => $date])}}">
                            {{date('d', strtotime($date))}} @lang('site.'.date('F', strtotime($date)))

                        </a>
                    </h5>
                </div>
            </div>
        @endforeach
    </div>

    <div class="sidebar-widget mb-30">
        <h3 class="post-title">Media</h3>
        @foreach($news as $post)
            <div class="blog-media-list mb-30 media" style="border-bottom: 1px dashed #ccc;">
                {{--<div class="post-thumb mr-4 mb-1">
                    <div class="date">
                        <p class="day">{{date('d', strtotime($post->date))}}</p>
                        <p class="month">{{date('M', strtotime($post->date))}}</p>
                    </div>
                </div>--}}
                <div class="media-body">
                    <h5 class="sub-title mb-1"><a href="{{route('post',['slug' => $post->slug])}}">{{$post->title}}</a></h5>
                </div>
            </div>
        @endforeach
    </div>



    <div class="sidebar-widget mb-30">
        @foreach($mainSites as $site)
            <div class="blog-media-list  p-1 mb-10 media main-site ">
                <div class="post-thumb mr-1">
                    <a href="//{{$site->url}}" target="_blank"><img src="{{Voyager::image($site->image)}}"
                                     style="height: auto; width: 70px;" alt="img"></a>
                </div>
                <div class="media-body">
                    <h4 class=" mb-1" style="font-size: 16px;"><a target="_blank" href="//{{$site->url}}">{{$site->title}} </a></h4>
                    <a href="//{{$site->url}}" target="_blank" class="date">{{$site->url}}</a>
                </div>
            </div>
        @endforeach

    </div>
</div>
