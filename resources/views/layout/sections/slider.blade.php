<section class="bg-light position-relative">
    <div class="main-slider dots-style theme1">
        @foreach($sliders as $slider)
            <div class="slider-item " style="background: url({{Voyager::image($slider->image)}}); background-size: cover; background-repeat: no-repeat;">
                <div class="container">
                    <div class="row align-items-center slider-height2">
                        <div class="col-12">
                            <div class="slider-content position-absolute bg-dark p-3 " style="left: 0; right: 0; bottom: 0;">
                                <h4>
                                    <a href="{{route('post',['slug' => $slider->slug])}}" class="text-white">{{$slider->title}}</a>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- slider-item end -->
        @endforeach

    </div>
    <!-- slick-progress -->
    <div class="slick-progress">
        <span></span>
    </div>
    <!-- slick-progress end-->
</section>
