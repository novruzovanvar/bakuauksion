<div class="common-banner bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-30">
                <div class="banner-thumb">
                    <a href="/template/shop-grid-4-column.html" class="zoom-in d-block overflow-hidden">
                        <img src="/template/assets/img/banner/15.jpg" alt="banner-thumb-naile">
                    </a>
                </div>
            </div>
            <div class="col-md-6 mb-30">
                <div class="banner-thumb mb-30">
                    <a href="/template/shop-grid-4-column.html" class="zoom-in d-block overflow-hidden">
                        <img src="/template/assets/img/banner/16.jpg" alt="banner-thumb-naile">
                    </a>
                </div>
                <div class="banner-thumb">
                    <a href="/template/shop-grid-4-column.html" class="zoom-in d-block overflow-hidden">
                        <img src="/template/assets/img/banner/17.jpg" alt="banner-thumb-naile">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
