@php
    $i =0;
@endphp
@foreach($sections as $key => $section)
@if($section->posts()->count() == 0)  @continue  @endif

@php $i++; @endphp
    <div class="popular-section theme1 pb-5  {{ $i%2 == 0 ? 'bg-dark' :'bg-white' }}  pt-80 ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center mb-30">
                        <h2 class="title {{ $i%2 == 0 ? 'text-white' :'text-dark' }} text-capitalize">{{$section->name}}</h2>
                        <p class="text mt-10"></p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="popular-slider-init dots-style">

                        @foreach($section->posts as $post)
                            <div class="slider-item">
                                <div class="card popular-card popular-card-bg zoom-in d-block overflow-hidden">
                                    <div class="card-body">
                                        <a href="{{route('auction',['category_slug' => $section->slug, 'slug'=> $post->slug])}}" class="thumb-naile">
                                            <img class="d-block mx-auto" src="{{Voyager::image($post->thumbnail('cropped'))}}" alt="{{$post->title}}">
                                        </a>
                                        <h3 class="popular-title text-center">
                                            <a href="{{route('auction',['category_slug' => $section->slug, 'slug'=> $post->slug])}}"> {{$post->title}}  </a>
                                            <p class="text-center p-1"><span class="badge badge-success position-relative">{{str_replace(',',' ',number_format($post->price))}} AZN</span></p>
                                        </h3>

                                        <a href="{{route('auction',['category_slug' => $section->slug, 'slug'=> $post->slug])}}" class="btn shop-now-btn text-uppercase mt-25">Ətraflı</a>
                                    </div>
                                </div>
                            </div>
                            <!-- slider-item end -->

                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

@endforeach

