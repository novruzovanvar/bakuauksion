<ul class="main-menu d-flex ">

    @foreach($items as $menu_item)

        @if($menu_item->children()->count() > 0)
            <li>
                <a href="{{ $menu_item->link() }}" target="{{ $menu_item->target }}" >{{ $menu_item->title }} <i
                        class="ion-ios-arrow-down" style="font-size: 14px;"></i></a>
                <ul class="sub-menu">
                    @foreach($menu_item->children as $sub)
                        <li><a href="{{ $sub->link() }}" target="{{ $sub->target }}">{{ $sub->title }}</a></li>
                    @endforeach

                </ul>
            </li>
        @else

            <li class="{{ request()->is($menu_item->link()) ? 'active' : ''}} @if($menu_item->link() == '/all-auctions') bg-primary @endif  ">
                <a class="" href="{{ $menu_item->link() }}" target="{{ $menu_item->target }}"> @if($menu_item->link() == '/') <i class="icon-home"></i> @else{{  $menu_item->title }} @endif </a>
            </li>
        @endif
    @endforeach
        {{--<li class="{{ request()->is($menu_item->link()) ? 'active' : ''}} bg-success  ">
            <a  href="{{ route('forArmyPosts') }}" style="font-size: 18px; color: #fff;"> Ordumuza dəstək kampaniyası  </a>
        </li>--}}

</ul>
