<ul >

    @foreach($items as $menu_item)

        @if($menu_item->children()->count() > 0)
            <li>
                <a href="{{ $menu_item->link() }}" target="{{ $menu_item->target }}" ><span class="menu-text">{{ $menu_item->title }} </span></a>
                <ul class="offcanvas-submenu">
                    @foreach($menu_item->children as $sub)
                        <li><a href="{{ $sub->link() }}" target="{{ $sub->target }}">{{ $sub->title }}</a></li>
                    @endforeach

                </ul>
            </li>
        @else

            <li class="{{ request()->is($menu_item->link()) ? 'active' : ''}}  ">
                <a href="{{ $menu_item->link() }}" target="{{ $menu_item->target }}"> <span class="menu-text"> {{  $menu_item->title }}</span> </a>
            </li>
        @endif
    @endforeach
        <li class="{{ request()->is($menu_item->link()) ? 'active' : ''}} bg-success  ">
            <a  href="{{ route('forArmyPosts') }}" style="font-size: 18px; color: #fff;"> <span class="menu-text">Ordumuza dəstək kampaniyası </span> </a>
        </li>

</ul>
