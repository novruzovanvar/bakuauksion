<div class="brand-slider-section theme1" style="background-color:#ecd3d3 !important;">
    <div class="container-xl">
        <div class="row">
            <div class="pt-40">
                <h1 class="section-header text-dark">Növbəti Hərraclar</h1>
                <div class="next-auctions-init  pt-35 pb-35 slick-nav-brand px-50">
                    @foreach($nextAuctions as $date)
                        <div class="slider-item">
                            <div class="single-brand px-10">
                                <a href="{{route('allAuctions',['start_date' => $date])}}" class="card ">
                                    <div class="card-body text-center">
                                        <h1> {{date('d', strtotime($date))}}</h1>
                                        <h3> @lang('site.'.date('F', strtotime($date)))</h3>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- slider-item end -->
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
