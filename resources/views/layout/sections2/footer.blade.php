<footer class="bg-light theme1 position-relative" id="footer">
    <!-- footer bottom start -->
    <div class="footer-bottom pt-80 pb-30">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 mb-30">
                    <div class="footer-widget mx-w-400">
                        <div class="footer-logo mb-35">
                            <a href="/" style="font-size: 1.5rem; display: flex; align-items: center;">
                                <img src="{{Voyager::image(setting('site.logo'))}}" style="max-width: 200px; margin-left: -50px;">
                                <span>{{setting('site.title')}}</span>
                            </a>
                        </div>
                        <p class="text mb-30">{!! setting('site.description') !!}</p>
                        <div class="address-widget mb-30">
                            <div class="media">
                                <span class="address-icon mr-3">
                                    <img src="/template/assets/img/icon/phone.png" alt="phone">
                                </span>
                                <div class="media-body">
                                    <p class="help-text text-uppercase">Əlaqə</p>
                                    @foreach(explode(',',setting('site.phone')) as $phone)
                                        <p class="title text-dark">
                                            <a href="tel:{{$phone}}">{{$phone}}</a>
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="social-network">
                            <ul class="d-flex">
                                <li><a href="{{setting('site.facebook')}}" target="_blank"><span
                                            class="icon-social-facebook"></span></a></li>
                                <li><a href="{{setting('site.twitter')}}" target="_blank"><span
                                            class="icon-social-twitter"></span></a></li>
                                <li><a href="{{setting('site.youtube')}}" target="_blank"><span
                                            class="icon-social-youtube"></span></a></li>
                                <li class="mr-0"><a href="{{setting('site.instagram')}}" target="_blank"><span
                                            class="icon-social-instagram"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2 mb-30">
                    <div class="footer-widget">
                        <div class="border-bottom cbb1 mb-25">
                            <div class="section-title pb-20">
                                <h2 class="title text-dark text-uppercase">İnformasİya</h2>
                            </div>
                        </div>
                        <!-- footer-menu start -->
                        <ul class="footer-menu">
                            <li><a href="/">Əsas səhifə</a></li>
                            <li><a href="/page/about">Haqqımızda</a></li>
                            <li><a href="/page/contact">Əlaqə</a></li>
                        </ul>
                        <!-- footer-menu end -->
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2 mb-30">
                    <div class="footer-widget">
                        <div class="border-bottom cbb1 mb-25">
                            <div class="section-title pb-20">
                                <h2 class="title text-dark text-uppercase">Bölmələr</h2>
                            </div>
                        </div>
                        <!-- footer-menu start -->
                        <ul class="footer-menu">
                            <li><a href="/auctions/dasinmaz-emlak">Daşınmaz əmlak</a></li>
                            <li><a href="/auctions/dasinar-emlak">Daşınar əmlak</a></li>
                            <li><a href="/auctions/neqliyyat">Nəqliyyat</a></li>
                            <li><a href="/auctions/qizil-zinet">Qızıl zinət</a></li>
                        </ul>
                        <!-- footer-menu end -->
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 mb-30">
                    <div class="footer-widget">
                        <div class="border-bottom cbb1 mb-25">
                            <div class="section-title pb-20">
                                <h2 class="title text-dark text-uppercase">Məlumat</h2>
                            </div>
                        </div>
                        <p class="text mb-20">Yeniliklərdən xəbərdar olmaq üçün abunə ol</p>
                        <div class="nletter-form mb-35">
                            <form class="form-inline position-relative"
                                  action="{{route('subscribe')}}#footer" method="post">
                                @csrf
                                <input class="form-control" name="email" type="text" placeholder="E-poçt">
                                @if(session('success'))
                                    <div class="pl-2 text-success">{{ session('success') }}</div>
                                @endif

                                @if(session('error'))
                                    <div class="pl-2 text-danger">{{ session('error') }}</div>
                                @endif
                                <button class="btn nletter-btn text-capitalize" type="submit">Göndər</button>
                            </form>
                        </div>

                        <div>
                            <div class="liveinternet">
                                <!--LiveInternet counter--><a href="https://www.liveinternet.ru/click"
                                                              target="_blank"><img id="licnt4AA8" width="88" height="120" style="border:0"
                                                                                   title="LiveInternet: number of visitors and pageviews is shown"
                                                                                   src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAEALAAAAAABAAEAAAIBTAA7"
                                                                                   alt=""/></a><script>(function(d,s){d.getElementById("licnt4AA8").src=
                                        "https://counter.yadro.ru/hit?t27.6;r"+escape(d.referrer)+
                                        ((typeof(s)=="undefined")?"":";s"+s.width+"*"+s.height+"*"+
                                            (s.colorDepth?s.colorDepth:s.pixelDepth))+";u"+escape(d.URL)+
                                        ";h"+escape(d.title.substring(0,150))+";"+Math.random()})
                                    (document,screen)</script><!--/LiveInternet-->

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer bottom end -->
    <!-- coppy-right start -->
    <div class="coppy-right pb-80">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="text-left">
                        <p class="mb-3 mb-md-0"> &copy; Bütün hüquqları qorunur </p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-8">
                    <div class="text-left">
                        {{setting('site.title')}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- coppy-right end -->
</footer>
