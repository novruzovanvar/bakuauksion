<div class="brand-slider-section theme1  pt-80">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="brand-init border-top py-35 slick-nav-brand">
                    @foreach($partners as $partner)
                    <div class="slider-item">
                        <div class="single-brand">
                            <a href="{{route('partner',['id' => $partner->id])}}" class="brand-thumb">
                                <img src="{{Voyager::image($partner->image)}}" alt="{{$partner->name}}" style="height: 150px;">
                            </a>
                        </div>
                    </div>
                    <!-- slider-item end -->
                        @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
