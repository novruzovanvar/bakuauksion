<!-- offcanvas-overlay start -->
<div class="offcanvas-overlay"></div>
<!-- offcanvas-overlay end -->
<!-- offcanvas-mobile-menu start -->
<div id="offcanvas-mobile-menu" class="offcanvas theme1 offcanvas-mobile-menu">
    <div class="inner">
        <div class=" mb-4 pb-4 text-right">
            <button class="offcanvas-close">×</button>
        </div>

        <nav class="offcanvas-menu">
            {!! menu('site', 'layout.sections.mob_menu') !!}
        </nav>
        <div class="offcanvas-social py-30">
            <ul>
                <li>
                    <a href="{{setting('site.facebook')}}"><i class="icon-social-facebook"></i></a>
                </li>
                <li>
                    <a href="{{setting('site.twitter')}}"><i class="icon-social-twitter"></i></a>
                </li>
                <li>
                    <a href="{{setting('site.instagram')}}"><i class="icon-social-instagram"></i></a>
                </li>
                <li>
                    <a href="{{setting('site.google')}}"><i class="icon-social-google"></i></a>
                </li>
                <li>
                    <a href="{{setting('site.youtube')}}"><i class="icon-social-youtube"></i></a>
                </li>
            </ul>
        </div>
    </div>
</div>

<header class="position-relative">
{{--    <div class="bg-light d-flex align-items-center py-3 " style="border-bottom: 1px solid #28a745;" >
        <marquee >
            <h3>
                <a style="color:red; padding: 0 10px;">
                    Sayt sınaq rejimində fəaliyyət göstərir.
                </a>
            </h3>
        </marquee>
--}}{{--
        <a href="{{route('forArmySubscribe')}}" class="btn btn--xl btn-success text-capitalize float-right">Elan yerləşdirmək üçün müraciət</a>
--}}{{--

    </div>--}}

    <!-- header-middle start -->
    <div class="header-middle bg-light  " style="border-bottom: 1px solid #000;">

        <div class="container-xl " >
            <div class="row align-items-center">
                <div class="col-sm-5 col-lg-4 col-xl-9 order-first">
                    <div class="logo text-center text-sm-left mb-30 mb-sm-0">
                        <div class="d-flex align-items-center">
                            <div >
                                <a href="/"  >
                                    <img src="{{Voyager::image(setting('site.logo'))}}" style="max-width: 200px; margin-left: -50px;">
                                </a>
                            </div>
                            <div >
                                <a href="/"  style="font-size: 2rem; line-height: 3rem;">
                                    <span>{{setting('site.title')}}</span>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-7 col-lg-8 col-xl-3 position-relative">
                    <div class="d-flex align-items-center justify-content-center justify-content-sm-end">
                        <div class="cart-block-links theme1">
                            <ul class="d-flex">

                                <li class="none-in-xxl">
                                    <a href="#" class="search-toggle">
                                        <span class="position-relative">
                                            <i class="ion-ios-search-strong"></i>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{setting('site.facebook')}}">
                                        <span class="position-relative">
                                            <i class="icon-social-facebook"></i>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a  href="{{setting('site.twitter')}}">
                                        <span class="position-relative">
                                            <i class="icon-social-twitter"></i>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a  href="{{setting('site.instagram')}}">
                                        <span class="position-relative">
                                            <i class="icon-social-instagram"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="mr-0 cart-block position-relative">
                                    <a  href="{{setting('site.youtube')}}">
                                        <span class="position-relative">
                                            <i class="icon-social-youtube"></i>
                                        </span>
                                    </a>
                                </li>
                                <!-- cart block end -->

                            </ul>


                        </div>
                        <div class="mobile-menu-toggle theme1 d-lg-none">
                            <a href="#offcanvas-mobile-menu" class="offcanvas-toggle">
                                <svg viewBox="0 0 800 600">
                                    <path d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200" id="top"></path>
                                    <path d="M300,320 L540,320" id="middle"></path>
                                    <path d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190" id="bottom" transform="translate(480, 320) scale(1, -1) translate(-480, -318)">
                                    </path>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="search-body" style="display: none;">
                        <div class="search-form">
                            <form action="{{route('allAuctions')}}" class="form-inline position-relative">
                                <input name="search" class="form-control theme1-border" type="search" placeholder="Axtar ...">
                                <button class="btn search-btn theme-bg btn-rounded" type="submit"><i class="icon-magnifier"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header-middle end -->

    <!-- header top start -->
    <div id="sticky" class="header-top theme2 bg-dark d-none d-lg-block">
        <div class="container-xl position-relative">
            <div class="row align-items-center">
                <div class="col-xl-12 col-lg-12 position-xl-relative">
                    <!-- header bottom start -->
                    <nav class="header-bottom">
                        {!! menu('site', 'layout.sections.main_menu') !!}

                    </nav>
                    <!-- header bottom end -->
                </div>
            </div>
        </div>
    </div>
    <!-- header top end -->

    <!--=======  End of category menu =======-->
</header>
