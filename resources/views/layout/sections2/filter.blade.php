<div>
    <form action="{{route('allAuctions')}}">
        <div class="d-flex align-items-center">
            <div class="flex-fill">
                <div class="form-group">
                    <label class="text-white">Kateqoriya</label>
                    <select name="category_id" id="" class="form-control" >
                        <option value="">Hamısı</option>
                        @foreach($sections as $section)
                            <optgroup label="{{$section->name}}">
                                @foreach($section->children as $sub)
                                    <option value="{{$sub->id}}" @if(request('category_id')==$sub->id) selected @endif>{{$sub->name}}</option>
                                @endforeach
                            </optgroup>

                        @endforeach
                    </select>
                </div>
            </div>
            <div class="flex-fill px-2">
                <div class="form-group">
                    <label class="text-white">Sifarişçi</label>
                    <select name="customer_id" id="" class="form-control">
                        <option value="">Hamısı</option>
                        @foreach($partners as $customer)
                            <option value="{{$customer->id}}" @if(request('customer_id')==$customer->id) selected @endif>{{$customer->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="flex-fill pr-2">
                <div class="form-group">
                    <label class="text-white">Tarix</label>
                    <select name="start_date" id="" class="form-control">
                        <option value="">Hamısı</option>
                        @foreach($nextAuctions as $date)
                            <option value="{{$date}}" @if(request('start_date')==$date) selected @endif>{{date('d.m.Y', strtotime($date))}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="flex-fill" style="max-width: 50px;">
                <button class="btn btn-success form-control" style="height: 38px;  margin-top: 5px;"><i class="fa fa-search"></i> </button>
            </div>
            {{--<div class="flex-fill">
                <a href="{{route('allAuctions')}}" class="btn btn-link form-control" style="height: 38px; max-width: 50px; margin-top: 5px;"><i class="fa fa-reply"></i> </a>
            </div>--}}

        </div>
    </form>
</div>
