<!--**********************************
        all css files
    *************************************-->

<!--***************************************************
   fontawesome,bootstrap,plugins and main style css
 ***************************************************-->

<link rel="stylesheet" href="/template/assets/css/fontawesome.min.css" />
<link rel="stylesheet" href="/template/assets/css/ionicons.min.css" />
<link rel="stylesheet" href="/template/assets/css/simple-line-icons.css" />
<link rel="stylesheet" href="/template/assets/css/plugins/jquery-ui.min.css">
<link rel="stylesheet" href="/template/assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="/template/assets/css/plugins/plugins.css" />
<link rel="stylesheet" href="/template/assets/css/style.css" />

<style>

    @font-face {
        font-family: "BebasNeue";
        font-style: normal;
        src: url("/template/assets/fonts/BebasNeue/BebasNeue-Book.otf") format("opentype");
    }

    .category-header{
        font-family: "BebasNeue";
        letter-spacing: 3px;
        font-size: 1.2rem;
    }

    .breadcrumb-section .list-group-item{
        font-size: 16px;
        font-weight: bold;
    }
    .section-header{
        font-family: "BebasNeue";
        letter-spacing: 3px;
        font-size: 2rem;
        text-align: center;
    }

    body, .popular-card .card-body .popular-title a, .main-menu li a , .footer-widget .section-title .title,.section-title .title {
        font-family: Helvetica, Arial, sans-serif;
    }

    .cart-block-links li a i {
        border: 1px solid #ccc;
        width: 40px;
        height: 40px;
        display: inline-block;
        border-radius: 100%;
        padding: 5px;
        font-size: 20px;
        line-height: 30px;
    }

    .link-card{
        height: 190px;
        background: #ddd;
        text-align: center;
        transition: all ease .5s;
        padding: 40px 15px;

    }

    .link-card:hover{
        background:#0d2f5d;
        cursor: pointer;
    }

    .link-card:hover a{
        color: #fff;
    }

    .link-card a {
        color: #0d2f5d;
        font-size: 14px;
        font-weight: 700;
    }

    .link-card i {
        font-size: 28px;
        font-size: 50px;
    }

    .main-site{
        border: 1px solid #ccc;
    }

    .post-thumb .date{
        width: 90px;
        height: 90px;
        background: #eee;
        text-align: center;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        color: #0d2f5d;
        transition: all ease .5s;
    }

    .media:hover .date{
        color: #fff;
        background: #0d2f5d;
    }

    .post-thumb .date .day{
        font-size: 24px;
    }

    .post-thumb .date .month{
        font-size: 18px;
    }

    .media-body .sub-title {
        font-size: 16px;
        color: #666;
        font-family: Helvetica, Arial, sans-serif;
        line-height: 22px;
    }

    .popular-card .card-body .popular-title {
        padding: 10px 0 10px 0;
        min-height: 105px;
    }

    .blog-ctry-menu li a{
        text-transform: none;
    }

    .slick-nav-brand .slick-prev, .slick-nav-brand .slick-next {
        opacity: 1;
        visibility: initial;
    }

    .next-auctions-init a.card{
        transition: .5s ease all;
    }

    .next-auctions-init a.card:hover{
        background: #0d2f5d !important;
        color: #eee;
    }

    .sub-menu li a {
        padding: 10px 0;
        line-height: 20px ;
    }

    .sub-menu  {
        min-width:400px;
    }

    .price-badge{
        left: 0;
        font-size: 16px;
    }
    .popular-card .card-title{
        padding: 15px;
        text-align: left;
        color: #333;
        font-size: 16px;
        line-height: 22px;
        height: 100px;
        overflow: hidden;
    }
    .popular-card-bg .card-body {
        position: relative;
        z-index: 1;
        padding: 0;
        border-radius: 15px;
        border: 1px solid #ccc;
        overflow: hidden;
    }
</style>

<!-- Use the minified version files listed below for better performance and remove the files listed above -->

<!--****************************
     Minified  css
****************************-->

<!--***********************************************
   vendor min css,plugins min css,style min css
 ***********************************************-->
<!-- <link rel="stylesheet" href="/template/assets/css/vendor/vendor.min.css" />
<link rel="stylesheet" href="/template/assets/css/plugins/plugins.min.css" />
<link rel="stylesheet" href="/template/assets/css/style.min.css" /> -->


