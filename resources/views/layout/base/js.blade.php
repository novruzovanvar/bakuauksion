<!--***********************
    all js files
 ***********************-->

<!--******************************************************
    jquery,modernizr ,poppe,bootstrap,plugins and main js
 ******************************************************-->

<script src="/template/assets/js/vendor/jquery-3.5.1.min.js"></script>
<script src="/template/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="/template/assets/js/popper.min.js"></script>
<script src="/template/assets/js/plugins/jquery-ui.min.js"></script>
<script src="/template/assets/js/bootstrap.min.js"></script>
<script src="/template/assets/js/plugins/plugins.js"></script>
<script src="/template/assets/js/main.js"></script>

<!-- Use the minified version files listed below for better performance and remove the files listed above -->

<!--***************************
      Minified  js
 ***************************-->

<!--***********************************
     vendor,plugins and main js
  ***********************************-->

<!-- <script src="/template/assets/js/vendor/vendor.min.js"></script>
<script src="/template/assets/js/plugins/plugins.min.js"></script>
<script src="/template/assets/js/main.js"></script> -->

<script>
    $(document).ready(function () {
        $(".next-auctions-init").slick({
            dots: false,
            arrows: true,
            infinite: true,
            prevArrow: '<button class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
            nextArrow: '<button class="slick-next"><i class="fas fa-arrow-right"></i></button>',
            slidesToShow: 4,
            slidesToScroll: 1,
            focusOnSelect: true,
            draggable: false
        });
    });
</script>
