@extends('layout.master')

@section('meta')
    @include('layout.base.meta')
@stop



@section('content')

    <!-- breadcrumb-section start -->
    <nav class="breadcrumb-section theme1 bg-light pt-50 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center mb-15">
                        <h2 class="title text-dark text-capitalize">Partnyorlar</h2>
                    </div>
                </div>

            </div>
        </div>
    </nav>
    <!-- breadcrumb-section end -->

    <!-- product tab start -->
    <div class="product-tab bg-white pt-80 pb-80">
        <div class="container-xl">
            <div class="row">
                @foreach($partners as $row)
                    <div class="col-md-3 mb-25">

                        <div class="product-thumb text-center">
                            <a href="{{route('partner',['id' => $row->id])}}">
                                <img src="{{Voyager::image($row->image)}}"
                                     style="height: 100px"
                                     title="{{$row->name}}"
                                     alt="product-thumb"></a>
                        </div>
                        <h5 class="pt-3 text-center">
                            <a href="{{route('partner',['id' => $row->id])}}">
                                {{$row->name}}
                            </a>
                        </h5>

                    </div>
                @endforeach
            </div>

        </div>
    </div>


    <!-- product tab end -->
@stop
