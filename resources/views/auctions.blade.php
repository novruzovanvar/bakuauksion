@extends('layout.master')

@section('meta')
    @include('layout.base.meta')
@stop

@section('content')

    <!-- breadcrumb-section start -->

    <!-- breadcrumb-section end -->

    <!-- product tab start -->
    <div class="product-tab bg-white pt-80 pb-80">
        <div class="container-xl">
            <div class="grid-nav-wraper bg-lighten2 mb-30">
                <div class="row align-items-center">
                    <div class="col-12 col-md-6 mb-3 mb-md-0">
                        <nav class="shop-grid-nav">
                            <ul class="nav nav-pills align-items-center" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                                       role="tab"
                                       aria-controls="pills-home" aria-selected="true">
                                        <i class="fa fa-th"></i>

                                    </a>
                                </li>
                                <li class="nav-item mr-0">
                                    <a class="nav-link " id="pills-profile-tab" data-toggle="pill"
                                       href="#pills-profile" role="tab" aria-controls="pills-profile"
                                       aria-selected="false"><i class="fa fa-list"></i></a>
                                </li>
                                <li><span
                                        class="total-products text-capitalize">{{$posts->count()}} Elan mövcuddur</span>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-12 col-md-6 position-relative">
                        {{--<div class="shop-grid-button d-flex align-items-center">
                            <span class="sort-by">Sort by:</span>
                            <button class="btn-dropdown rounded d-flex justify-content-between" type="button"
                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Relevance <span class="ion-android-arrow-dropdown"></span>
                            </button>
                            <div class="dropdown-menu shop-grid-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Relevance</a>
                                <a class="dropdown-item" href="#"> Name, A to Z</a>
                                <a class="dropdown-item" href="#"> Name, Z to A</a>
                                <a class="dropdown-item" href="#"> Price, low to high</a>
                                <a class="dropdown-item" href="#"> Price, high to low</a>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>

            <div>@include('layout.sections.filter')</div>
            <!-- product-tab-nav end -->
            <div class="tab-content" id="pills-tabContent">
                <!-- first tab-pane -->
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="row grid-view theme1">
                        @foreach($posts as $post)
                            <div class="col-sm-6 col-lg-4 col-xl-3 mb-30">
                                <div class="card popular-card popular-card-bg zoom-in d-block overflow-hidden">
                                    <div class="card-body">
                                        <p class="text-center p-1 position-absolute"><span
                                                class="badge badge-success position-relative">{{str_replace(',',' ',number_format($post->price))}} AZN</span>
                                        </p>

                                        <a href="{{route('auction',['category_slug' => $category->slug, 'slug'=> $post->slug])}}"
                                           class="thumb-naile">
                                            <img class="d-block mx-auto"
                                                 src="{{Voyager::image($post->thumbnail('cropped'))}}"
                                                 alt="{{$post->title}}">
                                        </a>
                                        <h3 class="popular-title text-center">
                                            <a href="{{route('auction',['category_slug' => $category->slug, 'slug'=> $post->slug])}}"> {{$post->title}}  </a>

                                        </h3>

                                        <a href="{{route('auction',['category_slug' => $category->slug, 'slug'=> $post->slug])}}"
                                           class="btn shop-now-btn text-uppercase mt-25">Ətraflı</a>
                                    </div>
                                </div>
                                <!-- product-list End -->
                            </div>
                        @endforeach

                    </div>
                </div>
                <!-- second tab-pane -->
                <div class="tab-pane fade " id="pills-profile" role="tabpanel"
                     aria-labelledby="pills-profile-tab">
                    <div class="row grid-view-list theme1">
                        @foreach($posts as $post)
                            <div class="col-12 mb-30">
                                <div class="card product-card">
                                    <div class="card-body">
                                        <div class="media flex-column flex-md-row">
                                            <div class="product-thumbnail position-relative">
                                                <span class="badge badge-danger top-right"></span>
                                                <a href="{{route('auction',['category_slug' => $category->slug, 'slug'=> $post->slug])}}"
                                                   class="thumb-naile">
                                                    <img class="d-block mx-auto"
                                                         src="{{Voyager::image($post->thumbnail('cropped'))}}"
                                                         alt="{{$post->title}}">
                                                </a>

                                            </div>
                                            <div class="media-body pl-30">
                                                <div class="product-desc py-0">
                                                    <h3 class="title">
                                                        <a href="{{route('auction',['category_slug' => $category->slug, 'slug'=> $post->slug])}}">
                                                            {{$post->title}}
                                                        </a>
                                                    </h3>
                                                    <div class="star-rating mb-10">
                                                        <span class="ion-ios-star"></span>
                                                        <span class="ion-ios-star"></span>
                                                        <span class="ion-ios-star"></span>
                                                        <span class="ion-ios-star"></span>
                                                        <span class="ion-ios-star de-selected"></span>
                                                    </div>
                                                    <h6 class="product-price">{{str_replace(',',' ',number_format($post->price))}}
                                                        AZN</h6>
                                                </div>
                                                <ul class="product-list-des">
                                                    <li>
                                                        {{$post->excerpt}}
                                                    </li>

                                                </ul>
                                                <div class="availability-list mb-30">
                                                    <p><strong>Son qeydiyyat tarixi:</strong>
                                                        <span class="badge position-static bg-success text-light rounded-0">{{ date('d.m.Y',strtotime($post->finish_date))}}</span>
                                                    </p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- product-list End -->
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>

            {!! $posts->links('vendor.pagination.custom') !!}

        </div>

    </div>


    <!-- product tab end -->
@stop
