@extends('layout.master')

@section('meta')
    @include('layout.base.meta')

    <script src="https://www.google.com/recaptcha/api.js?hl=az_AZ" async defer></script>

@stop

@section('content')

    <!-- breadcrumb-section start -->
    <nav class="breadcrumb-section theme1 bg-light pt-50 pb-50" style="background: url('/assets/img/bg_2.png');">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center mb-15">
                        <h2 class="title text-dark text-capitalize">Elan yerləşdirmək üçün müraciət</h2>
                    </div>
                </div>
                <div class="col-12">
                    <ol class="breadcrumb bg-transparent m-0 p-0 align-items-center justify-content-center">
                        <li class="breadcrumb-item"><a href="/">Əsas səhifə</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{route('forArmyPosts')}}">Ordumuza dəstək</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Elan yerləşdirmək üçün müraciət</li>
                    </ol>
                </div>
            </div>
        </div>
    </nav>
    <!-- breadcrumb-section end -->


    <section class="contact-section pt-80 pb-50">
        <div class="container-xl">
            <div class="card mb-50 border-success">

                <div class="card-body" style="font-size: 16px;">
                    <h3 class="contact-page-title">Qeyd*</h3>
                    {!! setting('site.for_army_description') !!}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-12 mb-30">



                    <!--  contact page side content  -->
                    <div class="contact-page-side-content">
                        <h3 class="contact-page-title">{!! setting('site.title') !!}</h3>
                        <p class="contact-page-message mb-30">{!! setting('site.description') !!}</p>
                        <!--  single contact block  -->

                        <div class="single-contact-block">
                            <h4><i class="fa fa-fax"></i> Ünvan</h4>
                            <p>{!! setting('site.address') !!}</p>
                        </div>

                        <!--  End of single contact block -->

                        <!--  single contact block -->

                        <div class="single-contact-block">
                            <h4><i class="fa fa-phone"></i> Telefon</h4>
                            @foreach(explode(',',setting('site.phone')) as $phone)
                            <p>
                                <a href="tel:{{$phone}}">{{$phone}}</a>
                            </p>
                            @endforeach
                        </div>

                        <!-- End of single contact block -->

                        <!--  single contact block -->

                        <div class="single-contact-block">
                            <h4><i class="fas fa-envelope"></i> E-poçt</h4>

                            @foreach(explode(',',setting('site.email')) as $email)
                                <p>
                                    <a href="{{$email}}">{{$email}}</a>
                                </p>
                            @endforeach
                        </div>

                        <!--=======  End of single contact block  =======-->
                    </div>

                    <!--=======  End of contact page side content  =======-->

                </div>
                <div class="col-lg-6 col-12 mb-30" id="contact-form">

                    <!--  contact form content -->
                    <div class="contact-form-content">
                        <h3 class="contact-page-title">Məlumatları daxil edin</h3>
                        <div class="contact-form">
                            @if(session('success'))
                                <div class="alert alert-success">{{ session('success') }}</div>
                            @endif

                            @if(session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @endif

                            <form  action="{{route('message')}}#contact-form" method="post">

                                @csrf

                                <div class="form-group">
                                    <label>Ad Soyad Ata adı <span class="required">*</span></label>
                                    <input type="text" name="name" id="name" value="{{old('name')}}" >
                                    @error('name')
                                    <div class="text-danger">Bu xana doldurulmalıdır.</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Mobil nömrə <span class="required">*</span></label>
                                    <input name="phone" id="phone" value="{{old('phone')}}" >

                                    @error('phone')
                                    <div class="text-danger">Bu xana doldurulmalıdır</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>E-poçt </label>
                                    <input type="email" name="email" id="email" value="{{old('email')}}" >

                                    @error('email')
                                    <div class="text-danger">Bu xana doldurulmalıdır</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>Elan haqqında</label>
                                    <textarea name="content" class="pb-10" id="content" >{{old('content')}}</textarea>

                                    <div class="g-recaptcha" data-sitekey="6LepIeAZAAAAALihyBEDMGePY3WYhGxQNVKyt388"></div>

                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" value="submit" id="submit" class="btn theme-btn--dark1 btn--lg"
                                            name="submit">Göndər</button>
                                </div>
                            </form>
                        </div>
                        <p class="form-messegemt-10"></p>
                    </div>
                    <!-- End of contact -->
                </div>
            </div>
        </div>
    </section>


@stop
