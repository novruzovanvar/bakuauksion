@extends('layout.master')

@section('meta')
    @include('layout.base.meta')
@stop

@section('content')

    <!-- breadcrumb-section start -->
    <nav class="breadcrumb-section theme1 bg-light pt-50 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center mb-15">
                        <h2 class="title text-dark text-capitalize">{{$category->name}}</h2>
                    </div>
                </div>

            </div>
        </div>
    </nav>
    <!-- breadcrumb-section end -->

    <div class="container-xl">
        <div class="row">
            <div class="col-12">
                <div class="sidebar-widget mb-30 mt-50">
                    <h3 class="post-title">{{$category->name}}</h3>
                    @foreach($posts as $post)
                        <div class="blog-media-list mb-30 media" style="border-bottom: 1px dashed #ccc;">
                            <div class="post-thumb mr-4 mb-1">
                                <div class="date">
                                    <p class="day">{{date('d', strtotime($post->date))}}</p>
                                    <p class="month">{{date('M', strtotime($post->date))}}</p>
                                </div>
                            </div>
                            <div class="media-body">
                                <h4 class=" mb-1"><a href="{{route('post',['slug' => $post->slug])}}">{{$post->title}}</a></h4>
                            </div>
                        </div>
                    @endforeach

                    {!! $posts->links('vendor.pagination.custom') !!}
                </div>
            </div>
        </div>
    </div>


    <!-- product tab end -->
@stop
