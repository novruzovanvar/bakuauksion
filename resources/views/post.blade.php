@extends('layout.master')

@section('meta')
    @include('layout.base.meta',[
        'title' => $post->seo_title,
        'description' => $post->meta_description,
        'image' => $post->image
    ])

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5fa4e64fed8380c6"></script>
@stop

@section('content')

    <!-- breadcrumb-section start -->
    <nav class="breadcrumb-section theme1 bg-light pt-50 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center mb-15">
                        <h2 class="title text-dark text-capitalize">{{$post->title}}</h2>
                    </div>
                </div>

            </div>
        </div>
    </nav>
    <!-- breadcrumb-section end -->

    <!-- product tab start -->
    <section class="about-section pt-80 pb-50" style="font-size: 16px;">
        <div class="container-xl">
            <div class="about-content">
                <h2 class="title mb-20">{{$post->excerpt}}</h2>
                {!! $post->body !!}

            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox mt-5"></div>
            </div>
        </div>
    </section>
    <!-- product tab end -->
@stop
