@extends('layout.master')

@section('meta')
    @include('layout.base.meta',[
        'title' => $partner->name,
        'description' => '',
        'image' => $partner->image
    ])

@stop

@section('content')

    <!-- breadcrumb-section start -->
    <nav class="breadcrumb-section theme1 bg-light pt-50 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center mb-15">
                        <h2 class="title text-dark text-capitalize">{{$partner->name}}</h2>
                    </div>
                </div>

            </div>
        </div>
    </nav>
    <!-- breadcrumb-section end -->

    <!-- product tab start -->
    <section class="about-section pt-80 pb-50" style="font-size: 16px;">
        <div class="container-xl">
            <div class="about-content row">
                <div class="col-3" style="border-right: 1px solid #666;">
                    <img src="{{Voyager::image($partner->image)}}" class="img-responsive"/>
                </div>

                <div class="col-9" >
                    <h5>{!! $partner->name !!}</h5>
                    {!! $partner->body !!}
                </div>


            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox mt-5"></div>
            </div>
        </div>
    </section>
    <!-- product tab end -->
@stop
