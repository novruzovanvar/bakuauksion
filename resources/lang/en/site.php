<?php

return [
    "DONE" => "Baş tutdu",
    "NOT_HAPPEN" => "Baş tutmadı",
    "WILL_HAPPEN" => "Keçiriləcək",
    "SOLD" => "Satıldı",
    "POSTPONE" => "Təxirə salındı",
    "January" =>'Yanvar',
    "February" =>'Fevral',
    "March" =>'Mart',
    "April" =>'Aprel',
    "May" =>'May',
    "June" =>'İyun',
    "July" =>'İyul',
    "August" =>'Avqust',
    "September" =>'Sentyabr',
    "November" =>'Noyabr',
    "October" =>'Oktyabr',
    "December" =>'Dekabr',
];
