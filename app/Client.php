<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Client extends Model
{

    public $additional_attributes = ['full_name'];

    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->surname} / tel: ({$this->phone})";
    }

}
