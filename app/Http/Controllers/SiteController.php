<?php

namespace App\Http\Controllers;

use App\ArmyCategory;
use App\ArmyPost;
use App\Customer;
use App\Email;
use App\Galery;
use App\Message;
use App\Partner;
use App\Site;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Category;
use TCG\Voyager\Models\Page;
use App\Post;

class SiteController extends Controller
{
    public function index()
    {
        $posts = Post::query();
        $posts = $posts->where('type', 'auction')
            ->published()
            ->whereIn('done',['WILL_HAPPEN','POSTPONE'])
            ->orderBy('start_date', 'asc')
            ->orderBy('id', 'desc')
            ->paginate(12);

        $sections = Category::query()
            ->with('children')
            ->whereIn('slug', ['dasinmaz-emlak', 'dasinar-emlak', 'neqliyyat'])
            ->get();

        $customers = Customer::all();

        $nextAuctions = Post::whereDate('start_date','>=', Carbon::today()->toDateString())->groupBy('start_date')->select('start_date')->pluck('start_date');

        $news = Post::where('type', 'news')->published()->orderBy('date', 'desc')->orderBy('id', 'desc')->take(5)->get([
            'id', 'title', 'slug', 'date'
        ]);


        $mainSites = Site::where('type', 'main')->orderBy('order', 'asc')->get();

        $partners = Partner::orderBy('order','asc')->get();

        return view('index', compact('partners',  'mainSites', 'news',  'posts', 'sections','customers','nextAuctions'));
    }

    public function test()
    {
        $posts = Post::query();

        if (\request('search')) {
            $posts->where('title', 'like', '%' . request('search') . '%');
        }

        if(\request('category_id')) $posts->where('category_id',request('category_id'));
        if(\request('customer_id')) $posts->where('customer_id',request('customer_id'));
        if(\request('start_date')) $posts->where('start_date',request('start_date'));


        $posts = $posts->where('type', 'auction')
            ->where('done','=','WILL_HAPPEN')
            ->orderBy('start_date', 'desc')
            ->orderBy('id', 'desc')
            ->published()
            ->paginate(12);

        $sections = Category::query()
            ->with('children')
            ->whereIn('slug', ['dasinmaz-emlak', 'dasinar-emlak', 'neqliyyat'])
            ->get();

        $news = Post::where('type', 'news')->published()->orderBy('date', 'desc')->take(5)->get([
            'id', 'title', 'slug', 'date'
        ]);

        $customers = Customer::all();

        $nextAuctions = Post::where('start_date','>',Carbon::now())->groupBy('start_date')->select('start_date')->pluck('start_date');


        $mainSites = Site::where('type', 'main')->orderBy('order', 'asc')->get();

        $partners = Partner::orderBy('order','asc')->get();

        return view('test', compact('partners','posts', 'sections','news','mainSites','nextAuctions','customers'));
    }

    public function auction($categorySlug, $slug)
    {
        $auction = Post::with('category')->where('slug', $slug)->published()->first();

        return view('auction', compact('auction'));
    }

    public function auctionById($id)
    {
        $auction = Post::with('category')
            ->where('id', $id)
            ->where('type', 'auction')
            ->published()
            ->firstOrFail();

        return view('auction', compact('auction'));
    }

    public function auctions($categorySlug)
    {
        $category = Category::query()
            ->where('slug', $categorySlug)
            ->first();

        $posts = Post::where('category_id', $category->id)
            ->where('type', 'auction')
            ->whereIn('done',['WILL_HAPPEN','POSTPONE'])
            ->orderBy('start_date', 'desc')
            ->orderBy('id', 'desc')
            ->published()
            ->paginate(12);

        $sections = Category::query()
            ->with('children')
            ->whereIn('slug', ['dasinmaz-emlak', 'dasinar-emlak', 'neqliyyat'])
            ->get();

        $news = Post::where('type', 'news')
            ->published()
            ->orderBy('date', 'desc')
            ->take(5)
            ->get(['id', 'title', 'slug', 'date']);

        $customers = Customer::all();

        $nextAuctions = Post::whereDate('start_date','>=', Carbon::today()->toDateString())->groupBy('start_date')->select('start_date')->pluck('start_date');


        $mainSites = Site::where('type', 'main')->orderBy('order', 'asc')->get();


        return view('auctions', compact('category', 'posts', 'sections', 'news','nextAuctions','customers','mainSites'));
    }

    public function categoryPosts($categorySlug)
    {
        $category = Category::query()
            ->where('slug', $categorySlug)
            ->first();

        $posts = Post::where('category_id', $category->id)->published()->orderBy('date', 'desc')->paginate(12);

        return view('category-posts', compact('category', 'posts'));
    }

    public function allAuctions()
    {
        $posts = Post::query();

        if (\request('search')) {
            $posts->where('title', 'like', '%' . request('search') . '%');
        }

        if(\request('category_id')) $posts->where('category_id',request('category_id'));
        if(\request('customer_id')) $posts->where('customer_id',request('customer_id'));
        if(\request('start_date')) $posts->where('start_date',request('start_date'));


        $posts = $posts->where('type', 'auction')
            ->where('done','=','WILL_HAPPEN')
            ->orderBy('start_date', 'desc')
            ->orderBy('id', 'desc')
            ->published()
            ->paginate(12);

        $sections = Category::query()
            ->with('children')
            ->whereIn('slug', ['dasinmaz-emlak', 'dasinar-emlak', 'neqliyyat'])
            ->get();

        $news = Post::where('type', 'news')->published()->orderBy('date', 'desc')->take(5)->get([
            'id', 'title', 'slug', 'date'
        ]);

        $customers = Customer::all();

        $nextAuctions = Post::where('start_date','>',Carbon::now())->groupBy('start_date')->select('start_date')->pluck('start_date');


        $mainSites = Site::where('type', 'main')->orderBy('order', 'asc')->get();

        $partners = Partner::orderBy('order','asc')->get();

        return view('all-auctions', compact('partners','posts', 'sections','news','mainSites','nextAuctions','customers'));
    }

    public function archiveAuctions()
    {
        $posts = Post::query();

        if (\request('search')) {
            $posts->where('title', 'like', '%' . request('search') . '%');
        }

        $posts = $posts->where('type', 'auction')
            ->where('done','!=','WILL_HAPPEN')
            ->published()
            ->paginate(12);

        $sections = Category::query()
            ->with('children')
            ->whereIn('slug', ['dasinmaz-emlak', 'dasinar-emlak', 'neqliyyat'])
            ->get();

        $news = Post::where('type', 'news')->published()->orderBy('date', 'desc')->take(5)->get([
            'id', 'title', 'slug', 'date'
        ]);

        $announces = Post::where('type', 'announce')->published()->orderBy('date', 'desc')->take(5)->get([
            'id', 'title', 'slug', 'date'
        ]);

        $mainSites = Site::where('type', 'main')->orderBy('order', 'asc')->get();

        return view('archive-auctions', compact('posts', 'sections','news','mainSites','announces'));
    }

    public function page($slug)
    {
        $page = Page::active()->where('slug', $slug)->first();

        return view('page', compact('page'));
    }

    public function post($slug)
    {
        $post = Post::where('slug', $slug)->first();

        return view('post', compact('post'));
    }

    public function partner($id)
    {
        $partner = Partner::where('id', $id)->first();

        return view('partner', compact('partner'));
    }

    public function contactUs()
    {

        return view('contact-us');
    }

    public function message(Request $request)
    {

        $captcha=$_POST['g-recaptcha-response'] ?? null;

        if(!$captcha){
            return back()->with('error', 'Captcha seçilməyib')->withInput();
        }
        $secretKey = "6LepIeAZAAAAAOhElIGWROW_BrGfTHgAM8JpWw_2";
        $ip = $_SERVER['REMOTE_ADDR'];
        // post request to server
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response,true);
        // should return JSON with success as true
        if($responseKeys["success"]) {
            $validate = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validate->fails()) {
                return back()->withErrors($validate->errors());
            }

            $data = $request->only('name', 'email', 'subject', 'content','phone');

            $message = Message::create($data);

            if ($message) {
                return back()->with('success', 'Müraciət göndərildi');
            }
        } else {
            echo '<h2>You are spammer ! Get the @$%K out</h2>';
            return false;
        }

        return back()->with('error', 'Xəta baş verdi')->withInput();
    }

    public function subscribe(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'email' => 'required',
        ]);

        if ($validate->fails()) {
            return back()->withErrors($validate->errors());
        }

        $data = $request->only('email');

        $email = Email::firstOrCreate($data);

        if ($email) {
            return back()->with('success', 'Abunə oldunuz. Təşəkkür edirik');
        }

        return back()->with('error', 'Xəta baş verdi');
    }

    public function forArmyPosts()
    {
        $categories = ArmyCategory::query()
            ->with('children')
            ->whereNull('parent_id')
            ->get();

        $posts = ArmyPost::query();

        if (\request('search')) {
            $posts->where('title', 'like', '%' . request('search') . '%');
        }
        if (\request('category_id')) {
            $posts->where('category_id', request('category_id'));
        }
        $posts = $posts->published()->paginate(12);

        return view('for-army-posts',compact('categories','posts'));
    }

    public function forArmyPost($slug)
    {
        $post = ArmyPost::where('slug', $slug)->published()->first();

        return view('for-army-post', compact('post'));
    }

    public function forArmySubscribe()
    {

        return view('for-army-subscribe');
    }

    public function gallery()
    {
        $gallery = Galery::all();

        return view('gallery', compact('gallery'));
    }

    public function partners()
    {
        $partners = Partner::all();

        return view('partners', compact('partners'));
    }
}
